#!/usr/bin/python3

import argparse
import os
import sys
import inspect
import shutil


currentdir = os.path.dirname(os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe()))))
sys.path.insert(0, os.path.dirname(currentdir)+"/source_encoding")
import binary_conversion as bc

sys.path.insert(0, os.path.dirname(currentdir)+"/synthesis_modules")
import dna_file_reader as dfr
import ordered_fragment_design as odf

"""
create clusters with known centroids

naive algorithm made quickly, no performances objective
"""


KMER_SIZE = 20

def cluster_score(fragment: str, centroid: str) -> float:
    """
    get the similarity score between the fragment and a centroid
    = nbr of common kmers
    all centroids needs to have the same length or the score will advantage longer ones
    """
    score = 0
    if abs(len(centroid)-len(fragment)) > 100: # too short/long fragments have 0
        return 0
    
    for i in range(len(centroid)-KMER_SIZE):
        kmer = centroid[i:i+KMER_SIZE]
        if kmer in fragment:
            score += 1
    return score


def sort_into_clusters(fragments_dict: dict, centroid_dict: dict, output_dir: str) -> None:
    """
    sort fragments into cluster with the most similarity score, some can be rejected
    """
    
    clusters_dict = dict.fromkeys(centroid_dict.keys(), []) # init a dict with every centroid as a key and empty list as value
    
    for name, fragment in fragments_dict.items():
        best_score, best_centroid = 0, ""
        for centroid_name in centroid_dict.keys():
            score = cluster_score(fragment, centroid_dict[centroid_name])
            if score >= best_score:
                best_score, best_centroid = score, centroid_name

        if best_score >= len(fragment)/5:
            clusters_dict[best_centroid] = clusters_dict[best_centroid] + [fragment]

    for cluster_name, cluster_list in clusters_dict.items():
        file_output = open(output_dir+"/"+cluster_name+".txt", "w")
        for sequence in cluster_list:
            #sequence = sequence[26:-26] #TODO REMOVE (for tampon+bsal)
            file_output.write(sequence+"\n")
        file_output.close()


# =================== main ======================= #
if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='sort fragments into clusters with known centroids')
    parser.add_argument('-i', action='store', dest='fragments_path', required=True,
                        help='path to the dna fragments')
    parser.add_argument('-c', action='store', dest='centroids_path', required=True,
                        help='path to the dna centroids')
    parser.add_argument('-o', action='store', dest='output_dir', required=True,
                        help='directory of the output clusters files')

    # ---------- input list ---------------#
    arg = parser.parse_args()

    print("clustering ...")    
    
    fragments_dict = dfr.read_fastq(arg.fragments_path)
    centroids_dict = dfr.read_fasta(arg.centroids_path)
    
    sort_into_clusters(fragments_dict, centroids_dict, arg.output_dir)    

    print("\tcompleted !")

