#!/bin/bash
set -u #exit and display error message if a variable is empty 

#Test performances to the channel coding


./performances_tests.sh documents/100_cd.txt test_100 100 true
./performances_tests.sh documents/100_nocd.txt test_100 100 false

./performances_tests.sh documents/200_cd.txt test_200 200 true
./performances_tests.sh documents/200_nocd.txt test_200 200 false

./performances_tests.sh documents/500_cd.txt test_500 500 true
./performances_tests.sh documents/500_nocd.txt test_500 500 false

./performances_tests.sh documents/1000_cd.txt test_1000 1000 true
./performances_tests.sh documents/1000_nocd.txt test_1000 1000 false

exit 0
