#!/usr/bin/python3

import argparse


def similarity_score(d_bin: str, r_bin: str) -> float:
    """
    get the similarity score between 2 binary sequences (depends of common bits)
    since any random seq can have a score of ~50%,
    the result is 2*(x-.5)
    so having 50% of common bits is a result of 0    
    """
    sum = 0
    for d_bit, r_bit in zip(d_bin, r_bin):
        if d_bit == r_bit:
            sum += 1
    
    score = max(0, 2*((sum/len(r_bin))-0.5))
    return score    
    

def compare_binaries(decoded_binary_path: str, reference_binary_path: str, output_path: str, n_read: int) -> None:
    """
    compare decoded binaries to their reference sequence,
    append the similarity score to a result file
    """
    
    with open(decoded_binary_path) as file:
        decoded_binaries_fragments = file.readlines()
        decoded_binaries_fragments = [fragment.rstrip() for fragment in decoded_binaries_fragments]
        
    with open(reference_binary_path) as file:
        reference_binaries_fragments = file.readlines()
        reference_binaries_fragments = [fragment.rstrip() for fragment in reference_binaries_fragments]
            
    if len(decoded_binaries_fragments) != len(reference_binaries_fragments):
        print("error : different number of decoded binaries from reference")
        exit(0)

    total_score = 0
    for (d_bin, r_bin) in zip(decoded_binaries_fragments, reference_binaries_fragments):
        score = similarity_score(d_bin, r_bin)
        total_score += score
    
    comparison_score = round(total_score/len(decoded_binaries_fragments),2)
    
    with open(output_path, "a+") as file:
        file.write(str(n_read) + "_" + str(comparison_score) + "\n")


# =================== main ======================= #
if __name__ == '__main__':
    
    parser = argparse.ArgumentParser(description='compare 2 binaries')
    parser.add_argument('-i', action='store', dest='decoded_binary_path', required=True,
                        help='path to the decoded binaries fragments')
    parser.add_argument('-r', action='store', dest='reference_binary_path', required=True,
                        help='path to the original binaries fragments')
    parser.add_argument('-o', action='store', dest='output_path', required=True,
                        help='path to the output results')
    parser.add_argument('-n', action='store', dest='n_read', required=True,
                        type=int, help='number of reads')
    
    # ---------- input list ---------------#
    arg = parser.parse_args()

    print("comparing binaries...")    
    
    compare_binaries(arg.decoded_binary_path, arg.reference_binary_path, arg.output_path, arg.n_read)
    
    print("\tcompleted !")

