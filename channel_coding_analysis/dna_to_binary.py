#!/usr/bin/python3

import argparse
import os
import sys
import inspect


currentdir = os.path.dirname(os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe()))))
sys.path.insert(0, os.path.dirname(currentdir)+"/source_encoding")
import binary_conversion as bc
sys.path.insert(0, os.path.dirname(currentdir)+"/synthesis_modules")
import dna_file_reader as dfr


def fragments_to_binary(input_path: str, output_path: str, n_read=None):
    """
    encode each binary fragment to a dna fragment
    """
    
    with open(input_path) as file_input:
        dna_fragments = file_input.readlines()
        dna_fragments = [fragment.rstrip() for fragment in dna_fragments]
            
    if n_read is not None:     
        dna_fragments = dna_fragments[:n_read] 
    
    with open(output_path, "w+") as file_output:
        
        for fragment in dna_fragments:
            binary_fragment = bc.dna_to_binary_z(fragment)
            file_output.write(binary_fragment + "\n")


def fasta_to_binary(input_path: str, output_path: str):
    fragments_dict = dfr.read_fasta(input_path)
    
    with open(output_path, "w+") as file_output:
        
        for fragment in fragments_dict.values():
            binary_fragment = bc.dna_to_binary_z(fragment)
            file_output.write(binary_fragment + "\n")
    


# =================== main ======================= #
if __name__ == '__main__':
    
    parser = argparse.ArgumentParser(description='convert dna fragments to binaries')
    parser.add_argument('-i', action='store', dest='input_path', required=True,
                        help='path to the directory of clustered fragments/consensus file')
    parser.add_argument('-o', action='store', dest='output_path', required=True,
                        help='path to the output directory of binaries/file of binaries')
    parser.add_argument('-n', action='store', dest='n_read', required=False,
                        type=int, default=None, help='number of fragments to convert (default=all)')


    # ---------- input list ---------------#
    arg = parser.parse_args()

    print("dna to binary...")    
    
    if os.path.isdir(arg.input_path):
        # if input is a directory, apply conversion on all sequences files within the dir
        for filename in os.listdir(arg.input_path):
            f = os.path.join(arg.input_path, filename)
            # checking if it is a file
            if os.path.isfile(f):
                output_file = os.path.join(arg.output_path, filename)
                fragments_to_binary(f, output_file, arg.n_read)
    else:
        fasta_to_binary(arg.input_path, arg.output_path)
        
    print("\tcompleted !")

