#!/usr/bin/python3

import argparse
import sys
import os
import inspect
import math

currentdir = os.path.dirname(os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe()))))
sys.path.insert(0, os.path.dirname(currentdir)+"/source_encoding")
import binary_conversion as bc


"""
convert a document into binary fragments
"""

PRIMER_LENGTH = 20 # length of primer
DNA_TO_BIN = 1.6 # conversion rate from dna to binary (5bases = 8bits)


def binary_fragmentation(doc_path: str, output_path: str, dna_fragment_length: int, channel_coding: int) -> None:
    """
    split the document in binaries fragments
    uses length of dna fragment to determine the needed binary length
    """
    
    if not os.path.isfile(doc_path):
        print("error in source encoding, input file not found at :",doc_path)
        exit(1)
        
    # read the lines as bytes
    with open(doc_path, "rb") as f:
        byte_list = []
        byte = f.read(1)
        while byte:
            byte_list.append(bin(ord(byte)))
            byte = f.read(1)
    # convert the bytes into binaries
    binary_payload = ""
    for x in byte_list:
        binary_char = x[2:]
        #add some 0 at the beginning of binaries shorter than 8
        while len(binary_char) < 8:
            binary_char = "0"+binary_char
        binary_payload+=binary_char
    
    if channel_coding == 1:
        binary_frag_length = int(((dna_fragment_length * DNA_TO_BIN) - 4)/2) # ((b*2) +4 )/1.6 = p # for channel coding
        binary_border_frag_length = math.floor(binary_frag_length - (PRIMER_LENGTH * DNA_TO_BIN)/2) # 1st and last frag are shorter for the primer addition
    else:
        binary_frag_length = int(dna_fragment_length * DNA_TO_BIN)
        binary_border_frag_length = math.floor(binary_frag_length - (PRIMER_LENGTH * DNA_TO_BIN))
 
    filtered_binary_payload = bc.apply_binary_filter(binary_payload)

    FRAGMENT_PER_GROUP = 10 # number of fragment in each sub assembly

    file_output = open(output_path, "w")

    cut_index = 0    
    group_size = 0
    continue_cut = True
    
    while(continue_cut):
        if group_size == 0 or group_size == FRAGMENT_PER_GROUP-1:
            next_cut_index = cut_index+binary_border_frag_length
        else:
            next_cut_index = cut_index+binary_frag_length
        
        if len(filtered_binary_payload) > next_cut_index:
            file_output.write(filtered_binary_payload[cut_index:next_cut_index]+"\n")
            cut_index = next_cut_index
            if group_size == FRAGMENT_PER_GROUP-1:
                group_size = 0
            else:
                group_size += 1
        else:
            continue_cut = False

    file_output.close()


# =================== main ======================= #
if __name__ == '__main__':
    
    parser = argparse.ArgumentParser(description='convert a document into binaries fragments')
    parser.add_argument('-i', action='store', dest='input_path', required=True,
                        help='path to the input document')
    parser.add_argument('-o', action='store', dest='output_path', required=True,
                        help='path to the output binary fragments')
    parser.add_argument('-f', action='store', dest='frag_length', required=True,
                        type=int, help='final length of dna fragments')
    parser.add_argument('--cd', action='store', dest='channel_coding', required=True,
                        type=int, help='choice of applying channel coding to the output binaries (to set the correct needed fragment size)')

    # ---------- input list ---------------#
    arg = parser.parse_args()
    

    binary_fragmentation(arg.input_path, arg.output_path, arg.frag_length, arg.channel_coding)

