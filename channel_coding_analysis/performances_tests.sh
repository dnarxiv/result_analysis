#!/bin/bash
set -u #exit and display error message if a variable is empty 

#Test performances to the channel coding

project_dir="$(dirname ${BASH_SOURCE})/../.." #parent*2 of the directory containing this script


call_function() {
	#call the cript passed in parameters, save it in logs
	#end the program if the called script has returned an error
	
	function_command=$@
	$function_command #execute the command
	if [ ! $? = 0 ] #test if command failed
	then
		echo "error in $(basename $1)"
		echo "canceling performances test"
		exit 1
	fi
}

if [ "$#" != 4 ]
then
	echo "error invalid number of arguments, ./performances_tests.sh input_name output_name frag_length channel_coding(True/False)"
	exit 1
fi

input_name=$1
output_name=$2
frag_length=$3
channel_coding=$4

n_synth=100

result_dir="$output_name"
mkdir -p $result_dir


#---Encoding---#

binary_fragmentation_script="$project_dir"/result_analysis/channel_coding_analysis/doc_to_binary.py
binary_frags="$result_dir/1_binary_frags"
if [ "$channel_coding" = true ]
then
	call_function "$binary_fragmentation_script" -i "$input_name" -o "$binary_frags" -f $frag_length --cd $channel_coding
else
	call_function "$binary_fragmentation_script" -i "$input_name" -o "$binary_frags" -f $frag_length
fi


#---Channel_Coding---#

if [ "$channel_coding" = true ]
then
	channel_encoding_script="$project_dir"/channel_coding/concat_CRC_CC/Concat_CRC_CC_Enc.jl 
	encoded_binaries_frags="$result_dir/2_encoded_binaries"
	call_function julia "$channel_encoding_script" "$binary_frags" "$encoded_binaries_frags" "0"
fi


#---DNA_Coding---#

binary_to_dna_script="$project_dir"/result_analysis/channel_coding_analysis/binary_to_dna.py
dna_frags="$result_dir/3_dna"
if [ "$channel_coding" = true ]
then
	call_function "$binary_to_dna_script" -i "$encoded_binaries_frags" -o "$dna_frags" -n 100
else
	call_function "$binary_to_dna_script" -i "$binary_frags" -o "$dna_frags" -n 100
fi


#----Sequencing & Basecalling----#

simu_seq_bc_script="$project_dir"/sequencing_modules/sequencing_basecalling_simulator/sequencing_basecalling_simulator.jl
sequenced_reads="$result_dir/4_synthesis_simulation"
call_function "$simu_seq_bc_script" "$dna_frags"_synthesis "$sequenced_reads"


#----Clustering----#

cluster_dir="$result_dir/5_clusters"
rm -rf "$cluster_dir"
mkdir -p "$cluster_dir"
clustering_script="$project_dir"/result_analysis/channel_coding_analysis/K_clustering.py
call_function "$clustering_script" -i "$sequenced_reads" -c "$dna_frags"_fragments -o "$cluster_dir"


for n_read in {1..100..2}
do

	if [ "$channel_coding" = true ]
	then
		#----DNA_Decoding----#
		
		dna_to_binary_script="$project_dir"/result_analysis/channel_coding_analysis/dna_to_binary.py
		binary_cluster_dir="$result_dir/6_binaries_clusters"
		rm -rf "$binary_cluster_dir"
		mkdir -p "$binary_cluster_dir"
		call_function "$dna_to_binary_script" -i "$cluster_dir" -o "$binary_cluster_dir" -n $n_read
		
		
		#----Channel_Decoding----#
	
		channel_decoding_script="$project_dir"/channel_coding/concat_CRC_CC/Concat_CRC_CC_Dec.jl 
		decoded_binaries_dir="$result_dir/7_decoded_binaries"
		rm -rf "$decoded_binaries_dir"
		mkdir -p "$decoded_binaries_dir"
		encoded_len=$(cat "$encoded_binaries_frags" | awk 'NR==1 { print length($0); }') # get length of an encoded binary line
		
		for file_name in $(find "$binary_cluster_dir" -maxdepth 1 -mindepth 1 -type f -exec basename {} ';') ; do
			call_function julia "$channel_decoding_script" "$binary_cluster_dir"/$file_name "$decoded_binaries_dir"/$file_name $encoded_len "0" #TODO auto len
		done
		
		# concatenate results into one file
		concatened_binaries="$result_dir/8_decoded_binary_file"
		cat $(find "$decoded_binaries_dir" -name "*.txt" | sort -V) > "$concatened_binaries"
	else
		
		#---Consensus---#
		
		consensus_script="$project_dir"/sequencing_modules/cluster_consensus.py
		dna_consensus="$result_dir/6_dna_consensus"
		call_function "$consensus_script" -i "$cluster_dir" -o "$dna_consensus" -f $frag_length -n $n_read
		
		#----DNA_Decoding----#
		
		dna_to_binary_script="$project_dir"/result_analysis/channel_coding_analysis/dna_to_binary.py
		concatened_binaries="$result_dir/8_decoded_binary_file"
		call_function "$dna_to_binary_script" -i "$dna_consensus" -o "$concatened_binaries"
		
	fi
	
	#----Result_analysis----#
	
	analysis_script="$project_dir"/result_analysis/channel_coding_analysis/compare_binaries.py
	results="$result_dir/9_results_"$frag_length"_"$channel_coding
	call_function "$analysis_script" -i "$concatened_binaries" -r "$binary_frags" -o "$results" -n "$n_read" 

done

exit 0
