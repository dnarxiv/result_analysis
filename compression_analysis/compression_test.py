#!/usr/bin/python3

import argparse
import os
import subprocess
import math
from pathlib import Path
import time




def gzip_file(file_path, output_path, decompression=False):
    """
    compress the file with gzip and write it at the output path
    """
    if decompression:
        compression_command = "gzip -dkc < "+ file_path + " > " + output_path.replace(".gz", "")
        subprocess.run('/bin/bash -c "$COMMAND"', shell=True, env={'COMMAND': compression_command})
        return
        
    if not os.path.isfile(output_path+".gz"):
        compression_command = "gzip -c9 "+ file_path + " > "+output_path+".gz"
        subprocess.run('/bin/bash -c "$COMMAND"', shell=True, env={'COMMAND': compression_command})
    else:
        print("already done",output_path)
    

def cmix_file(file_path, output_path, decompression=False):
    
    if decompression:
        decompression_command = "./cmix/cmix -d "+ file_path + " "+output_path.replace(".cx", "")
        subprocess.run('/bin/bash -c "$COMMAND"', shell=True, env={'COMMAND': decompression_command})
        return
    
    if not os.path.isfile(output_path+".cx"):
        compression_command = "./cmix/cmix -c "+ file_path + " "+output_path+".cx"
        subprocess.run('/bin/bash -c "$COMMAND"', shell=True, env={'COMMAND': compression_command})
    else:
        print("already done",output_path)
    
    
def nncp_file(file_path, output_path, decompression=False):
    if not os.path.isfile(output_path+".np"):
        compression_command = "./nncp/nncp --cuda c "+ file_path + " "+output_path+".np"
        subprocess.run('/bin/bash -c "$COMMAND"', shell=True, env={'COMMAND': compression_command})
    else:
        print("already done",output_path)
    

def unzip_file(file_path):
    """
    unzip the file and write it just where it is
    """
    decompression_command = "gzip -d "+ file_path
    subprocess.run('/bin/bash -c "$COMMAND"', shell=True, env={'COMMAND': decompression_command})


def compress_all(files_dir_path: str, compressed_dir_path: str, compression: str) -> None:
    """
    create a dir of zip files corresponding to every file
    """
    try:
        os.mkdir(compressed_dir_path)
    except OSError as error:
        print(error)
        
    start_time = time.time()
    for filename in os.listdir(files_dir_path):
        file_path = os.path.join(files_dir_path, filename)
        compressed_file_path = os.path.join(compressed_dir_path, filename)

        # checking if it is a file
        if os.path.isfile(file_path):
            if compression == "nncp":
                nncp_file(file_path, compressed_file_path)
                
            if compression == "cmix":
                cmix_file(file_path, compressed_file_path)
                
            if compression == "gzip":
                gzip_file(file_path, compressed_file_path)
        
        elif os.path.isdir(file_path):
            print("error compress_all : subdirectory found in input dir path", filename)
            exit(0)
    print("compression time :", str(round(time.time() - start_time, 3)),"s")
    
    
def uncompress_all(compressed_dir_path: str, decompressed_dir_path: str, compression: str) -> None:
    try:
        os.mkdir(decompressed_dir_path)
    except OSError as error:
        print(error)
        
    start_time = time.time()    
    for filename in os.listdir(compressed_dir_path):
        file_path = os.path.join(compressed_dir_path, filename)
        decompressed_file_path = os.path.join(decompressed_dir_path, filename)
        # checking if it is a file
        if os.path.isfile(file_path):
            if compression == "nncp":
                nncp_file(file_path, decompressed_file_path, decompression=True)
                
            if compression == "cmix":
                cmix_file(file_path, decompressed_file_path, decompression=True)
                
            if compression == "gzip":
                gzip_file(file_path, decompressed_file_path, decompression=True)
        
        elif os.path.isdir(file_path):
            print("error compress_all : subdirectory found in input dir path", filename)
            exit(0)
    print("decompression time :", str(round(time.time() - start_time, 3)),"s")
    

def get_compression_rate(files_dir_path, compressed_dir_path):
    """
    get the size of original and compressed dirs
    """

    input_size = sum(f.stat().st_size for f in Path(files_dir_path).glob('**/*') if f.is_file())

    output_size = sum(f.stat().st_size for f in Path(compressed_dir_path).glob('**/*') if f.is_file())
    
    formatted_input_size = str(round(input_size/1000, 1))+" ko"
    formatted_output_size = str(round(output_size/1000, 1))+" ko"

    print(formatted_input_size,"->",formatted_output_size)
    print("compression ratio :",str(round(output_size/input_size,3)))


# =================== main ======================= #
if __name__ == '__main__':
    
    parser = argparse.ArgumentParser(description='pre process files for dna storage before binary conversion')
    parser.add_argument('-i', action='store', dest='input_dir_path', required=True,
                        help='path to input directory of files (no subdirs) to store')
    parser.add_argument('-c', action='store', dest='compression', required=True,
                        help='algorithm for compression to test')
    parser.add_argument('-d', dest='decompression', required=False,
                        default=False, action='store_true', help='parameter for decompression')


    # ---------- input list ---------------#
    arg = parser.parse_args()
    

    if not arg.decompression:
        print("compressing files with", arg.compression, "...") 
        output_dir_path = "compressed_files/"+os.path.basename(arg.input_dir_path)+"_"+arg.compression

        compress_all(arg.input_dir_path, output_dir_path, arg.compression)
        get_compression_rate(arg.input_dir_path, output_dir_path)
    
    else:
        print("decompressing files with", arg.compression, "...")
        output_dir_path = "decompressed_files/"+os.path.basename(arg.input_dir_path)
        uncompress_all(arg.input_dir_path, output_dir_path, arg.compression)
        
    print("\tcompleted !")

