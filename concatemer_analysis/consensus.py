
CC = {'A':'T','C':'G','G':'C','T':'A'}

def reverse_complement(seq):
    rcseq = ""
    i = len(seq)-1
    while i >= 0:
        rcseq += CC[seq[i]]
        i -= 1
    return rcseq

def canonic_kmer(k):
    ck = reverse_complement(k)
    if k < ck:
        return k
    else:
        return ck

# generate a dictionary of canonic kmers from a sequence
def seq2kmer(seq,kmer_size):
    kmer_dict = {}
    for i in range(len(seq)-kmer_size+1):
        k = canonic_kmer(seq[i:i+kmer_size])
        if k in kmer_dict:
            kmer_dict[k] += 1
        else:
            kmer_dict[k] = 1
    return kmer_dict

RESULTS = []
COUNT = 0
ended_cases = [0,0,0] # counter for return cases in the consensus : 0 count, 1 len, 2 success


def consensus(KMER,solid,path,end,lseq):
    global RESULTS
    global COUNT
    global ended_cases
    COUNT += 1
    kmer_size = len(end)
    if COUNT > 10000: # too deep in recursion
        ended_cases[0] += 1
        return
    if len(path) > lseq : # too long, could start to loop indefinitly
        ended_cases[1] += 1
        return
    if path[-kmer_size:] == end and len(path) == lseq: # stop condition : reached the end part and correctly sized
        if path not in RESULTS:
            RESULTS.append(path)
        ended_cases[2] += 1
        return

    for nt in ['A','C','G','T']:
        k = canonic_kmer(path[-kmer_size+1:]+nt)
        if k in KMER:
            if KMER[k] > solid:
                consensus(KMER,solid,path+nt,end,lseq)
    return
                


def compute_consensus(seq,start_kmer,end_kmer,target_len):
    """
    get a list of consensus of a single sequence of concatemers
    """
    global RESULTS
    global COUNT
    global ended_cases
    RESULTS = []
    COUNT = 0
    solid = int(len(seq)/1000) # solid = kmer minimum occurrences, ignore non frequent kmers

    kmer_size = len(start_kmer)
    KMER = seq2kmer(seq,kmer_size)
    # compute consensus : results are put in global variable RESULTS
    consensus(KMER,solid,start_kmer,end_kmer,target_len)
    #print(ended_cases)
    return RESULTS
