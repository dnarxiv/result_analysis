import sys
import numpy as np

"""
apply error correction code
"""

# Read the PCM contained in filename (alist format)

convertion_dict = {"A": "00", "C": "01", "G": "10", "T": "11"}


def read_alist(filename: str):

	# read input file
    f = open(filename, 'r')
    line = f.readline().split(" ")

    # read dimensions
    N = int(line[0])
    M = int(line[1])

    # init empty matrix
    H = np.zeros((N,M))

    dv = np.zeros(N)
    dc = np.zeros(M)

    # Read the degrees
    f.readline()
    line = f.readline().split(" ")
    for j in range(N):
        dv[j] = int(line[j])

    line = f.readline().split(" ")
    for k in range(M):
        dc[k] = int(line[k])

    # read the parity check matrix
    for j in range(N):
        line = f.readline().split(" ")
        for k in range(int(dv[j])):
            pos = int(line[k])-1 # positions in the matrix file starts at 1
            H[j][pos] = 1

    # Close the file
    f.close()

    return H


def apply_correction_code(sequence):

    if len(sequence) != 64:
        print("correction code : invalid sequence length",str(len(sequence)))
        return 0

    # convert sequence to binary
    binary_sequence = ""
    for nt in sequence:
        binary_sequence += convertion_dict[nt]

    # convert to numpy array
    binary_array = np.array(list(binary_sequence[:-1]), dtype=int) # the correction code only apply on 127 bits = remove last bit

    result = np.dot(H_matrix.T, binary_array) % 2 # scalar product between transpose of H and the sequence vector

    is_valid = np.all(result == 0)
    
    return is_valid


H_matrix = read_alist("correction_code/BCH_127_113_2_strip.alist")

input_path = sys.argv[1]
apply_correction_code(input_path)
