import sys

ff = open(sys.argv[1])

nb_sub = 0
nb_gap = 0
align_len = 0
line = ff.readline()

ref_length = 64
errors_nbr_dict = {}

while line != "":
    ll = line.split()
    x = int(ll[8]) # get start of alignment
    y = int(ll[9]) # get end of alignment
    xx = min(x,y) # reverse if start > end
    yy = max(x,y) # same

    if xx==1 and yy==ref_length: # if alignment if complete
        align_len += ref_length
        sub = int(ll[4]) # mismatch
        gap = int(ll[5]) # gap
        nb_sub += sub
        nb_gap += gap
        errors_nbr_dict[sub+gap] = errors_nbr_dict.get(sub+gap, 0)+1
    line = ff.readline()
ff.close()

print(errors_nbr_dict)
print ("% sub :",(nb_sub*100)/align_len)
print ("% gap :",(nb_gap*100)/align_len)
