import sys
import matplotlib.pyplot as plt



def concat_count_stat(filename_in):
    """
    make some stats on the concatemer_counter
    """
    total_reads = 347873 # number of total init reads (the concat number odesn't show the zeros
    occ_dict = {}
    count = 0
    ff = open(filename_in)
    new_line = ff.readline()
    while new_line != "":
        occ = int(new_line.split(" ")[2].replace("\n",""))
        occ_dict[occ] = occ_dict.get(occ, 0) +1
        count += 1
        new_line = ff.readline()
    occ_dict[0] = total_reads-count # get the number of reads that has 0 concatemers

    max_occ = max(list(occ_dict.keys()))
    occ_list = []
    for i in range(max_occ+1):
        occ_list.append(occ_dict.get(i, 0))

    print(occ_list)


def length_distribution(input_fasta, input_aligned_fasta=None):
    """
    display the length distribution of reads, optionnaly the unaligned reads
    """

    ff = open(input_fasta)
    lines = ff.readlines()
    ff.close()

    nbr_mol = int(len(lines)/2)
    length_distribution = {}

    length_interval = 25 # size of the interval of length columns

    for i in range(nbr_mol):

        sequence = lines[2*i+1].replace("\n","")

        seq_length = length_interval*(len(sequence)//length_interval)

        length_distribution[seq_length] = length_distribution.get(seq_length, 0) + 1 # +1 (number of read) +seq_length (DNA content)

    ax = plt.gca()
    ax.set_xlim([0, 2000])#max(length_distribution.keys())+10])
    ax.set_ylim([0, 5])
    plt.bar(list(length_distribution.keys()), [100*k/nbr_mol for k in length_distribution.values()], length_interval, color='r')


    if input_aligned_fasta is not None: # also print the aligned reads distribution on top of the full reads

        ff = open(input_aligned_fasta)
        lines_aligned = ff.readlines()
        ff.close()

        nbr_mol_aligned = int(len(lines_aligned)/2)
        length_distribution_aligned = {}

        for i in range(nbr_mol_aligned):

            sequence = lines_aligned[2*i+1].replace("\n","")

            seq_length = length_interval*(len(sequence)//length_interval)

            length_distribution_aligned[seq_length] = length_distribution_aligned.get(seq_length, 0) + 1 # +1 (number of read) +seq_length (DNA content)


        plt.bar(list(length_distribution_aligned.keys()), [100*k/nbr_mol for k in length_distribution_aligned.values()], length_interval, color='b')

    plt.title("length of reads distribution for _")
    plt.show()



intput_path = sys.argv[1]
output_path = sys.argv[2]


length_distribution(intput_path, output_path)
#concat_count_stat(intput_path)

