import sys


"""
conversion of sequences files
"""

def fastq_to_fasta(fastq_path: str):
    """
    convert a .fastq file to a .fasta file
    """

    if not ".fastq" in fastq_path:
        print("bad input for fastq_to_fasta : "+fastq_path)
        exit(0)

    fasta_path = fastq_path.replace(".fastq",".fasta")


    ff = open(fastq_path, 'r')
    fo = open(fasta_path, 'w')
    com = ff.readline()
    nb_seq = 0
    while com != "":
        seq_name = com.split(" ")[0][1:] # remove space and tabs from id
        seq_name = seq_name.split("\t")[0]

        seq = ff.readline()
        fo.write(">"+seq_name+"\n"+seq)
        #fo.write(">seq_"+str(nb_seq)+"\n"+seq)
        ff.readline()
        ff.readline()
        com = ff.readline()
        nb_seq += 1
    ff.close()
    fo.close()
    print ("translated",nb_seq,"reads to",fasta_path)


def txt_to_fasta(txt_path: str):
    """
    convert a txt to fasta by removing newlines in middle of sequences
    """

    if not ".txt" in txt_path:
        print("bad input for txt_to_fasta : "+txt_path)
        exit(0)

    fasta_path = txt_path.replace(".txt",".fasta")

    ff = open(txt_path)
    fo = open(fasta_path,"w")
    new_line = ff.readline()
    nb_seq = 0
    while new_line != "":
        fo.write(new_line) # write seq name
        seq = ""
        new_line = ff.readline()
        while not new_line.startswith(">") and new_line != "":
            seq += new_line.replace("\n","")
            new_line = ff.readline()
        fo.write(seq+"\n") # write seq name
        nb_seq += 1
    ff.close()
    fo.close()
    print ("convert to fasta",nb_seq,"reads to",fasta_path)


def csv_to_fasta(csv_path:str):
    """
    convert a csv to fasta
    """
    if not ".csv" in csv_path:
        print("bad input for txt_to_fasta : "+csv_path)
        exit(0)

    fasta_path = csv_path.replace(".csv",".fasta")
    ff = open(csv_path)
    fo = open(fasta_path,"w")
    new_line = ff.readline()
    nb_seq = 0
    while new_line != "":
        fo.write(">reference_"+str(nb_seq)+"\n") # write seq name
        seq = new_line.split(",")[2]
        fo.write(seq+"\n")
        new_line = ff.readline()
        nb_seq += 1
    ff.close()
    fo.close()
    print ("convert to fasta",nb_seq,"reads to",fasta_path)


def filter_fasta(input_fasta, output_fasta):
    """
    filter the reads from a fasta
    """

    with open(input_fasta, 'r') as input, open(output_fasta, 'w') as output:
        line = input.readline()
        while line != "":
            seq_id = line.replace("\n","")
            seq = input.readline().replace("\n","").replace("N","")
            #if 2025 <= len(seq) <= 2200:
            output.write(seq_id+"\n"+seq+"\n")
            line = input.readline()



intput_path = sys.argv[1]
if len(sys.argv) > 2:
    input2 = sys.argv[2]


fastq_to_fasta(intput_path)
#txt_to_fasta(intput_path)
#csv_to_fasta(intput_path)

#filter_fasta(intput_path, input2)


#get_ref_presence(intput_path, input2)
