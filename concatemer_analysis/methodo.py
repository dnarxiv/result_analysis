import sys
import os
import time
import random
import math
import inspect
import consensus as cs
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
sys.path.insert(0, os.path.dirname(os.path.dirname(currentdir))+"/sequencing_modules")
import reads_consensus_class

REPETITIONS = 10 # number of times to test the same methodo

start_kmer = "AATTAAAGGA"      # right-end of the left zone before the insert
end_kmer   = "TAAGGTTTAA"      # left-start of the tight zone after the insert
start_kmer_rvc = "TCCTTTAATT"
end_kmer_rvc   = "TTAAACCTTA"



def print_ref_per_read(ref_per_reads_list, output_path):

    """ compute the mean of the {REPETITIONS} lists in input
    """
    max_length = max(len(list_k) for list_k in ref_per_reads_list)
    extended_ref_lists = []

    for i in range(REPETITIONS):
        list_i = ref_per_reads_list[i]
        extended_ref_lists.append(list_i + [list_i[-1]] * (max_length-len(list_i))) # extend the shorter lists with their last elements

    average_list = [sum(lst[i] for lst in extended_ref_lists) / REPETITIONS for i in range(max_length)]

    with open(output_path, 'w') as output:
        for item in average_list:
            output.write(f"{item}\n")


def add_errors(sequence, error_rate):
    """
    add some ins/del/subs errors in the sequence
    """
    seq_result = ""
    for i in range(len(sequence)):
        if random.random() < error_rate:
            error_type = random.choices(['insert', 'del', 'subst'], weights=[1, 2, 1], k=1)[0]
            if error_type == 'insert': # add a random base before
                seq_result += random.choice('ATCG')
                seq_result += sequence[i]
            elif error_type == 'del': # don't add the base
                continue
            elif error_type == 'subst': # change the base into a random value
                seq_result += random.choice('ATCG')
        else:
            seq_result += sequence[i] # add the base without errors
    return seq_result


def create_random_readfile(input_path, output_path, added_error_rate, output_length=None):
    """
    create a file of random extracted reads
    input is a fasta of reads
    added errors to create additionnal errors in the reads
    output length is same as input as default (= number of reads)
    """

    with open(input_path, 'r') as f:
        lines = f.readlines()
    reads_number = len(lines) //2

    if output_length is None: # the output is just a random shuffle of input
        output_length = reads_number

    elif output_length > reads_number:
        print("error random sample file : too much selected reads ("+str(output_length)+") for a file of "+str(reads_number))

    #print("creating random file of "+str(output_length)+" reads...")

    # group lines pairs
    pairs = [lines[i:i+2] for i in range(0, len(lines), 2)]

    # shuffle pairs
    random.shuffle(pairs)

    with open(output_path, 'w') as file:
        for pair in pairs[:output_length]:
            # add errors in the read
            if added_error_rate > 0:
                sequence = pair[1][:-1]
                pair[1] = add_errors(sequence, added_error_rate) + "\n"
            file.writelines(pair)



def perfect_short_strands(references_list, reads_path, temp_reads_path, ref_per_reads_path, added_error = 0):
    """
    use a set of ref and reads where the refs are presents
    shuffle the reads and browse it until finding all the refs without errors
    can add additionnal errors to the used reads
    """
    print("\nperfect short strands method :\n")

    number_of_references = len(references_list)

    ref_per_read = [] # save the number of references get per number of reads
    reads_to_complete_list = [] # save the number of reads needed to get all references
    # apply the method multiple time to get an average
    for i in range(REPETITIONS):
        ref_per_read.append([])

        references_found_dict = {} # dict of found correct references

        shuffled_file_path = temp_reads_path+"/random_reads_"+str(i)+".fasta"
        create_random_readfile(reads_path, shuffled_file_path, added_error) # shuffle the reads
        reads_to_complete = 0 # number of reads needed to find all the references without errors in the set

        with open(shuffled_file_path, 'r') as shuffled_reads:

            new_line = shuffled_reads.readline() # skip read name
            while new_line != "":
                reads_to_complete += 1

                read_seq = shuffled_reads.readline()[:-1] # remove line jump

                for ref_seq in references_list:
                    if ref_seq in read_seq:
                        references_found_dict[ref_seq] = references_found_dict.get(ref_seq, 0) +1
                        break

                ref_per_read[i].append(len(references_found_dict))
                #print(str(reads_to_complete)+" reads : "+str(len(references_found_dict))+" references found")

                if len(references_found_dict) >= 128:
                    break

                new_line = shuffled_reads.readline() # skip read name

        reads_to_complete_list.append(reads_to_complete)
        print("perfect short strands : "+str(reads_to_complete)+" reads to get "+str(len(references_found_dict))+" perfect references")
    total_reads = sum(reads_to_complete_list)
    print_ref_per_read(ref_per_read, ref_per_reads_path)
    print("\nperfect short strands, average of "+str(round(total_reads/len(reads_to_complete_list),2))+" reads to get 128 perfect references\n")


def consensus_short_strands(references_list, reads_path, temp_reads_path, ref_per_reads_path, added_error = 0):
    """
    apply consensus on increasing number of reads until getting all references
    """
    print("\nconsensus short strands method :\n")

    target_len = len(references_list[0]) + 2*len(start_kmer) # length of a correct consensus

    temp_reads_file = temp_reads_path+"/temp_reads.fasta"

    ref_per_read = [] # save the number of references get per number of reads

    reads_to_complete_list = [] # save the number of reads needed to get all references
    # apply the method multiple time to get an average
    for i in range(REPETITIONS):
        ref_per_read.append([])

        references_found_dict = {} # dict of found correct references

        shuffled_file_path = temp_reads_path+"/random_reads_"+str(i)+".fasta"
        create_random_readfile(reads_path, shuffled_file_path, added_error) # shuffle the reads

        with open(shuffled_file_path , 'r') as f:
            shuffled_read_lines = f.readlines()

        # try to apply consensus with an increasing number of reads
        for reads_number in range(10, 10000, 10):
            used_lines = shuffled_read_lines[0:2*reads_number]
            with open(temp_reads_file, 'w') as temp:
                temp.write("".join(used_lines))

            consensus_dict = reads_consensus_class.kmer_consensus(temp_reads_file, "", start_kmer, reads_consensus_class.dfr.reverse_complement(end_kmer), 20, 2, target_len)
            for cons_seq in consensus_dict.values():
                for ref_seq in references_list:
                    if ref_seq in cons_seq:
                        references_found_dict[ref_seq] = references_found_dict.get(ref_seq, 0) +1
                        break

            for k in range(10): # add 10 times since the loop is every 10 reads
                ref_per_read[i].append(len(references_found_dict))
            #print(str(reads_number)+" reads : "+str(len(references_found_dict))+" references found")

            if len(references_found_dict) >= 128:
                break

        print("consensus short strands : "+str(reads_number)+" reads to get "+str(len(references_found_dict))+" perfect references")
        reads_to_complete_list.append(reads_number)
    total_reads = sum(reads_to_complete_list)
    print_ref_per_read(ref_per_read, ref_per_reads_path)
    print("\nconsensus short strands, average of "+str(round(total_reads/len(reads_to_complete_list),2))+" reads to get 128 perfect references\n")


def concatemers(references_list, reads_path, temp_reads_path, ref_per_reads_path, added_error = 0):

    target_len = len(references_list[0]) + 2*len(start_kmer) # length of a correct consensus

    ref_per_read = [] # save the number of references get per number of reads

    reads_to_complete_list = [] # save the number of reads needed to get all references

    # apply the method multiple time to get an average
    for i in range(REPETITIONS):
        ref_per_read.append([])

        references_found_dict = {} # dict of found correct references

        shuffled_file_path = temp_reads_path+"/random_reads_"+str(i)+".fasta"
        create_random_readfile(reads_path, shuffled_file_path, added_error) # shuffle the reads

        # read sequences
        # --------------
        ff = open(shuffled_file_path) # fasta

        reads_number = 0
        new_line = ff.readline()

        while new_line != "":
            reads_number += 1

            seq = ff.readline()[:-1]
            new_line = ff.readline()

            """sys.stdout.write('\r')
            sys.stdout.write("apply consensus on read "+str(reads_number))
            sys.stdout.flush()"""

            if (start_kmer in seq or start_kmer_rvc in seq) and (end_kmer in seq or end_kmer_rvc in seq):
                pass
            else: # skip when no both extremities
                continue
            # get the consensus sequence of this read
            consensus_list = cs.compute_consensus(seq,start_kmer,end_kmer, target_len+2*len(start_kmer))
            if len(consensus_list) > 0 :
                #print(new_line[:-1],len(seq))
                #fo.write(new_line+seq+"\n")
                for consensus_seq in consensus_list:
                    s1 = consensus_seq[len(start_kmer):-len(start_kmer)] # consensus without primers
                    # check if consensus is a reference
                    if s1 in references_list:
                        references_found_dict[s1] = references_found_dict.get(s1, 0) +1 # increment the number of found for this ref
                        break


            ref_per_read[i].append(len(references_found_dict))
            #print(str(reads_number)+" reads : "+str(len(references_found_dict))+" references found")

            if len(references_found_dict) >= 128:
                break
            if reads_number > 10000:
                break

        #print()
        print("concatemers : "+str(reads_number)+" reads to get "+str(len(references_found_dict))+" perfect references")
        reads_to_complete_list.append(reads_number)
    total_reads = sum(reads_to_complete_list)
    print_ref_per_read(ref_per_read, ref_per_reads_path)
    print("\nconcatemers, average of "+str(round(total_reads/len(reads_to_complete_list),2))+" reads to get 128 perfect references\n")


error_rate = 0.0

#methodo_path = "data/methodo_10dec24/"
methodo_path = "data/methodo_14jan25/"


ref_S1_path = methodo_path+ "S1/references_set_S1.fa"
ref_S255_path = methodo_path+ "S255/255_Seq_Elsa_NonModifie.fa" # to test consensus with erasure code

references_S1_list = [] # list of references
references_S255_list = [] # list of references

# extract list of references sequences
with open(ref_S1_path, 'r') as references:
    new_line = references.readline()
    while new_line != "":
        s = references.readline()[:-1] # remove line jump
        references_S1_list.append(s)
        new_line = references.readline()

with open(ref_S255_path, 'r') as references:
    new_line = references.readline()
    while new_line != "":
        s = references.readline()[:-1] # remove line jump
        references_S255_list.append(s)
        new_line = references.readline()


#---- methodo 10 dec 24 ----#
"""
reads_singles_S1_path = methodo_path+ "S1/aligned_single255_S1.fasta" # to test single frags
reads_singles_S255_path = methodo_path+ "S255/aligned_single255_S255.fasta" # to test single with erasure code
reads_RB12_S1_path = methodo_path+ "S1/aligned_RB12_S1.fasta" # to test consensus with only 128 refs
reads_RB12_S255_path = methodo_path+ "S255/aligned_RB12_S255.fasta" # to test consensus with erasure code

output_dir_path = methodo_path+ "random_reads_sets"

#perfect_short_strands(references_S1_list, reads_singles_S1_path, output_dir_path, methodo_path+ "refs_per_reads/perfect_short_128.txt", error_rate)
#perfect_short_strands(references_S255_list, reads_singles_S255_path, output_dir_path, methodo_path+ "refs_per_reads/perfect_short_255.txt", error_rate)

consensus_short_strands(references_S1_list, reads_singles_S1_path, output_dir_path, methodo_path+ "refs_per_reads/consensus_short_128.txt", error_rate)
consensus_short_strands(references_S255_list, reads_singles_S255_path, output_dir_path, methodo_path+ "refs_per_reads/consensus_short_255.txt", error_rate)

#concatemers(references_S1_list, reads_RB12_S1_path, output_dir_path, methodo_path+ "refs_per_reads/concatemers_128.txt", error_rate)
#concatemers(references_S255_list, reads_RB12_S255_path, output_dir_path, methodo_path+ "refs_per_reads/concatemers_255.txt", error_rate)
"""

#---- methodo 14 jan 25 ----#

temp_reads_path = methodo_path+ "random_reads_sets"


for basecall_type in ["Fast"]: # eval to get the associated variable name
    for set_type in ["S1"]:
        print("tests for "+basecall_type+"_"+set_type)
        ref_path = eval("references_"+set_type+"_list")
        reads_path = methodo_path + set_type +"/RB12_"+basecall_type+"_"+set_type+".fasta"
        #reads_path = methodo_path + set_type +"/short_reads_255_"+basecall_type+"_"+set_type+".fasta"

        #results_path = methodo_path + "results/RB12_"+basecall_type+"_"+set_type+".txt"
        results_path = methodo_path + "results/short_reads_255_"+basecall_type+"_"+set_type+".txt"

        concatemers(ref_path, reads_path, temp_reads_path, results_path)
        #perfect_short_strands(ref_path, reads_path, temp_reads_path, results_path)
        #consensus_short_strands(ref_path, reads_path, temp_reads_path, results_path)


#----
