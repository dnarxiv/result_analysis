import consensus as cs
import random
import math
import sys
import os
import time


# compute censensus
# -----------------
#
# usage
#
# python read2consensus.py reference reads 
#
#  - reference = fasta reference file
#  - reads     = fasta read file

RC_TAB = str.maketrans("ACTG", "TGAC")

def reverse_complement(sequence: str) -> str:
    return sequence.translate(RC_TAB)[::-1]


primer_len = 10
length_interval = 25 # size interval to print length distribution


def read2consensus(references_list, input_path, output_path):

    references_found_dict = {} # dict of found correct references

    target_len = len(references_list[0])
    start_kmer = references_list[0][:primer_len]
    end_kmer = references_list[0][-primer_len:]

    # read sequences
    # --------------
    ff = open(input_path) # fasta
    fo = open(output_path, 'a') # output.fasta

    correct_consensus = 0 # nbr of perfect consensus
    incorrect_consensus = 0 # nbr of bad consensus
    no_consensus = 0 # nbr of time zero consensus can be found
    length_distribution = {}

    line_number = 0
    new_line = ff.readline()
    print(references_list[0])

    hairpin_counter = 0
    while new_line != "":
        line_number += 1
        seq = ff.readline()[:-1]
        rounded_length = length_interval*(int(len(seq))//length_interval)
        length_distribution[rounded_length] = length_distribution.get(rounded_length, [0,0,0,0]) # 0: correct,1:not correct,2:not found,3: no extremities

        #sys.stdout.write('\r')
        #sys.stdout.write("apply consensus on read "+str(line_number))
        #sys.stdout.flush()

        if (start_kmer in seq or reverse_complement(start_kmer) in seq) and (end_kmer in seq or reverse_complement(end_kmer) in seq):
            hairpin_counter += 1
        else: # skip when no both extremities
            length_distribution[rounded_length][3] += 1
            new_line = ff.readline()
            continue
        # get the consensus sequence of this read
        consensus_list = cs.compute_consensus(seq, start_kmer, end_kmer, target_len)#+2*primer_len)
        if len(consensus_list) > 0 :
            #print(new_line[:-1],len(seq))
            correct_found = False
            #fo.write(new_line+seq+"\n")
            for consensus_seq in consensus_list:
                #s1 = consensus_seq[primer_len:-primer_len] # consensus without primers
                # check if consensus is a reference
                if consensus_seq in references_list:
                    correct_consensus += 1
                    correct_found = True
                    references_found_dict[consensus_seq] = references_found_dict.get(consensus_seq, 0) +1 # increment the number of found for this ref
                    length_distribution[rounded_length][0] += 1
                    break
            if not correct_found:
                length_distribution[rounded_length][1] += 1
                incorrect_consensus += 1
                #pass
        else:
            length_distribution[rounded_length][2] += 1
            no_consensus += 1
        new_line = ff.readline()
    print()
    print("references found :"+str(len(references_found_dict))+"/"+str(len(references_list)))
    ff.close()
    fo.write(os.path.splitext(os.path.basename(input_path))[0]+"\n"+str(correct_consensus)+"\t"+str(line_number)+"\n")
    fo.close()
    print("correct consensus :"+str(correct_consensus)+" ; incorrect :"+str(incorrect_consensus)+" ; not found :"+str(no_consensus))
    print(str(hairpin_counter)+" times both start & end kmers in seq")

    #consensus_by_length(length_distribution, output_path)




def consensus_by_length(length_distribution, output_path):
    """
    analysis on length depending on consensus results
    print a csv
    """

    max_len = max(length_distribution.keys())
    with open(output_path, 'w') as output:
        output.write("length;correct consensus;incorrect consensus;not found;no both ends\n")
        for possible_len in range(0, max_len+length_interval, length_interval):
            result_list = length_distribution.get(possible_len, [0,0,0,0])
            output.write(str(possible_len)+";"+";".join(str(k) for k in result_list)+"\n")



reads_path = "data/data_20jan25/aligned_reads_"
ref_path = "data/data_20jan25/ref_oligo_hairpin.fa"
ref_dict = {}

# read template sequences
ff = open(ref_path)
new_line = ff.readline()
while new_line != "":
    ref_name = new_line[1:-1]
    ref_seq = ff.readline()[:-1]
    ref_dict[ref_name] = ref_seq
    new_line = ff.readline()
ff.close()

for cond in ["RB21_70C", "RB22_64C", "RB23_60C"]:
    for file in sorted(os.listdir(reads_path+cond)):
        name = os.path.splitext(os.path.basename(file))[0]
        ref_list = [ref_dict[name]]
        print(cond+":"+name)
        read2consensus(ref_list, reads_path+cond+"/"+file, "data/data_20jan25/results_"+cond+"_2.txt")

#random_reads_path = input_path #create_random_readfile(input_path, 100000)


    



