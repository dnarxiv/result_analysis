#!/bin/bash

call_function() {
	#call the cript passed in parameters, save it in logs
	#end the program if the called script has returned an error
	
	function_command=$@
	$function_command #execute the command
	if [ ! $? = 0 ] #test if command failed
	then
		echo "error in $(basename $1)"
		echo "canceling multiple_tests"
		exit 1
	fi
}


complete_test() {
	# test the consensus from scratch
	n_frag=$1
	n_reads=$2
	script_name=$3
	
	call_function ./performances_tests.sh $script_name $n_frag  $n_reads
}


quick_tests() {
	# test the consensus when containers are already created and completed until the consensus step
	rm -f generated_files/*_*
	
	n_frag=$1
	n_reads=$2
	script_name=$3
	
	call_function ./test_reads_consensus.py -c "$(pwd)/containers/container_"$n_frag"_frags" -n $n_frag -r $n_reads --script $script_name
}

script_name=new_reads_dsk8 # reads/minia

quick_tests 10 500 $script_name
quick_tests 100 7000 $script_name
quick_tests 1000 100000 $script_name
quick_tests 10000 1500000 $script_name

#-------------- Exit --------------#
echo "___Fin du processus \!___"

exit 0

# scp -r oboulle@dnarxiv.irisa.fr:~/Documents/result_analysis/consensus_analysis/results_graphs/* ~/Documents/result_analysis/consensus_analysis/results_graphs
