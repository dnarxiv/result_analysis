#!/bin/bash
set -u #exit and display error message if a variable is empty 

#Test performances to the consensus

project_dir="$(dirname ${BASH_SOURCE})/../.." #parent*2 of the directory containing this script
commands_dir="$project_dir"/workflow_commands

source "$project_dir"/workflow_commands/metadata_manager.sh #load the xml manager script
source "$project_dir"/workflow_commands/log_manager.sh #load the log manager script


call_function() {
	#call the cript passed in parameters, save it in logs
	#end the program if the called script has returned an error
	
	function_command=$@
	log_script_call "$container_path" "$function_command"
	$function_command #execute the command
	if [ ! $? = 0 ] #test if command failed
	then
		echo "error in $(basename $1)"
		echo "canceling performances test"
		exit 1
	fi
}

if [ "$#" != 3 ]
then
	echo "error invalid number of arguments, ./performances_tests.sh script_name n_frag n_reads_max"
	exit 1
fi

script_name=$1
n_frag=$2
n_reads_max=$3

container_name="containers/container_"$n_frag"_frags"

container_path="$(pwd)/$container_name"

if [ -d "$container_path" ] 
then
    echo "a container named $container_name already exists"
    rm -rf "$container_path"
fi
mkdir -p "$container_path"


frag_length=60

init_metadata_file "$container_path" "$container_name" false $frag_length "ordered" false "no_spacer" "nanopore"


#---Generate Source---#

#create a fake document with the exact defined number of fragments
source_file="$container_path"/source.txt
echo aa > "$source_file"
for i in $(seq 2 $n_frag); do
	echo aaaaaaaaaaa  >> "$source_file"
done


#---Encoding---#

call_function "$commands_dir"/dna_add.sh -nocd "$container_path" "$source_file"


#---Synthesis---#

n_synth=100

call_function "$commands_dir"/dna_store.sh "$container_path" -n_synth $n_synth


#----Molecule Selection----# 

n_read=$(($n_frag * 300))
start_primer=$(get_doc_param $container_path 0 "start_primer")
stop_primer=$(get_doc_param $container_path 0 "stop_primer")

#call_function "$commands_dir"/dna_read.sh -n_read $n_read "$container_path" 0 $container_path/resultat_0.txt
molecule_selection_script="$project_dir"/sequencing_modules/select_molecules.py
selected_mol_path="$container_path"/0/6_select_mol.fasta
#select molecules from container molecules with the good primers
call_function "$molecule_selection_script" -i "$container_path"/container_molecules.fasta -o "$selected_mol_path" --start $start_primer --stop $stop_primer -n $n_read


#----Sequencing & Basecalling----#

simu_seq_bc_script="$project_dir"/sequencing_modules/sequencing_basecalling_simulator/sequencing_basecalling_simulator.jl
sequenced_reads_path="$container_path"/0/7_sequenced_reads.fastq
call_function "$simu_seq_bc_script" "$selected_mol_path" "$sequenced_reads_path"


#---Test Consensus---#

#clean old generated files
rm -f generated_files/consensus*

call_function ./test_reads_consensus.py -c "$container_path" -n $n_frag -r $n_reads_max --script $script_name

exit 0
