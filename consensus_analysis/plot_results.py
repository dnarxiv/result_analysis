import os
import sys
import matplotlib.pyplot as plt
import ast



def extract_values(results_path):
    
    results_file = open(results_path)
    reads_array = ast.literal_eval(results_file.readline())
    values_array = ast.literal_eval(results_file.readline())
    results_file.close()
    
    return reads_array, values_array
    

def plot_precision(reads_array, precision_array, color, label):
    plt.figure(1)
    plt.plot(reads_array, precision_array, color=color, label=label)
    
    plt.xlabel("n_reads")
    plt.ylabel("precision (%)")

    plt.ylim(0,105)
    plt.xlim(left=0)
    #plt.xlim(right=500)
    plt.legend()


def plot_time(reads_array, time_array, color, label): 
    #plt.figure(1)
    plt.plot(reads_array, time_array, color=color, label=label)
    
    plt.xlabel("n_reads")
    plt.ylabel("consensus_time (s)")

    plt.ylim(bottom=0)
    plt.ylim(top=13)
    plt.xlim(left=0)
    #plt.xlim(right=500)


# =================== main ======================= #
if __name__ == '__main__':
    
    if len(sys.argv) < 2 or len(sys.argv) > 5:
        print("usage : plot_results.py results_path [results_path_2] [results_path_3] [results_path_4]")
        sys.exit(1)
    
    colors = ['red', 'blue', 'green', 'orange']
    labels = ['Python', 'DSK', 'Minia', 'MindTheGap']
    #colors = ['blue', 'green', 'orange']
    
    for i in range(len(sys.argv)-1):
        results_path = sys.argv[i+1]
        
        reads_array, values_array = extract_values(results_path)
        print(values_array)
        
        if "precision" in results_path:
            plot_precision(reads_array, values_array, colors[i], labels[i])
        elif "time" in results_path:
            plot_time(reads_array, values_array, colors[i], labels[i])
    
    plt.show()
