#!/usr/bin/python3

import os
import sys
import inspect
import argparse
import random
import matplotlib.pyplot as plt
import subprocess
import time

currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)

sys.path.insert(0, os.path.dirname(parentdir)+"/synthesis_modules")
import dna_file_reader as dfr

sys.path.insert(0, os.path.dirname(parentdir)+"/sequencing_modules/")
import reads_consensus
import reads_consensus_class

sys.path.insert(0, parentdir+"/fragment_assembly_analysis/")
import alignment_global as ag


def get_random_reads(read_file: str, result_file: str, n_reads: int, min_read_size = 0, max_read_size = 10000) -> None:
        
    init_reads = dfr.read_fastq(read_file)
    
    read_pool = []
    
    for read_name, sequence in init_reads.items():
        if len(sequence) >= min_read_size and len(sequence) <= max_read_size:
            read_pool.append((read_name, sequence))
    
    if len(read_pool) < n_reads: print("not enough reads in pool ("+str(len(read_pool))+"), needed :",str(n_reads))
    random_reads = random.sample(read_pool, min(len(read_pool), n_reads))
    
    
    output_file = open(result_file,"w")
    for read_name, sequence in random_reads:
        output_file.write(read_name+"\n")
        output_file.write(sequence+"\n")
        output_file.write("+\n")
        output_file.write("---\n")
    output_file.close()


def get_precision(source: str, consensus_path: str, aligned=True) -> float:
    """
    return the resulting precision percentage
    """
    
    if not os.path.isfile(consensus_path):
        return 0
      
    results_dict = dfr.read_fasta(consensus_path)
    
    if results_dict == {}:
        return 0

    def compute_score(ref_seq: str, result_seq: str) -> float:
        # get the % of matching bases between 2 aligned sequences
        source_length = len(ref_seq)
        nbr_match = 0 # number of matching nucleotides between source and result
        for i in range(len(result_seq)):
            if i >= len(ref_seq):
                continue
            if ref_seq[i] == result_seq[i]:
                nbr_match += 1
        print("precision score : ", str(round(100*nbr_match/source_length, 2)))
        return round(100*nbr_match/source_length, 2)
    
    
    def compute_score_len(ref_seq: str, result_seq: str) -> float:
        # get the % of matching bases between 2 aligned sequences, jump the gaps
        source_length = len(ref_seq)
        if ref_seq == result_seq:
            print("precision score : 100.0 Perfect !")
        else:
            print("precision score : ", str(round(100*len(result_seq)/source_length, 2)))
        return round(100*len(result_seq)/source_length, 2)
    
    
    def compute_score_perfect(ref_seq: str, result_seq: str) -> float:
        if ref_seq == result_seq:
            #print("precision score : 100.0 Perfect !")
            return 100
        else:
            #print("precision score : not perfect...")
            return 0
    
    max_score = 0
    
    for consensus_sequence in results_dict.values():
        max_score = max(max_score, compute_score_perfect(source, consensus_sequence))
        
    print("precision score : ", str(max_score))
    return max_score
    
    """
    if aligned:
        # source and result are already aligned (for kmer consensus)
        #return compute_score_len(source, result)
        return compute_score_perfect(source, result)
    else:
        # try to align the sequences
        
        middle_source = source[len(source)//2:len(source)//2+30] # middle sequence of the source
        middle_source_rev = dfr.reverse_complement(source)[len(source)//2:len(source)//2+30]
        
        if middle_source in result: # if the middle sequence is in the consensus, use the index to align the consensus
            corrected_start_index = result.index(middle_source)-len(source)//2
            if corrected_start_index < 0:
                aligned_consensus = "_"*(-1*corrected_start_index) + result
            else:
                aligned_consensus = result[corrected_start_index:]
            #print(corrected_start_index, len(aligned_consensus))
            
            return compute_score(source, aligned_consensus)
        
        if middle_source_rev in result:
            corrected_start_index = result.index(middle_source_rev)-len(source)//2
            if corrected_start_index < 0:
                aligned_consensus = "_"*(-1*corrected_start_index) + result
            else:
                aligned_consensus = result[corrected_start_index:]
            #print(corrected_start_index, len(aligned_consensus))
            # compare the alignment score with the rev comp of the source
            return compute_score(dfr.reverse_complement(source), aligned_consensus)
        
        return 0
    """


def test_reads_consensus(selected_reads_file: str, expected_result_requence: str, min_occ: int) -> (float, float):
    """
    apply the kmer consensus algorithm on a defined number of reads and returns the precision and time
    """
        
    consensus_file = "generated_files/consensus_reads.fasta"

    start_time = time.time()
    
    start_primer, stop_primer = expected_result_requence[:20], dfr.reverse_complement(expected_result_requence[-20:])
    
    reads_consensus.kmer_consensus(selected_reads_file, consensus_file, start_primer, stop_primer, min_occ)
    
    stop_time = time.time() - start_time

    # read result to get accuracy
    precision = get_precision(expected_result_requence, consensus_file)

    return precision, stop_time


def test_reads_consensus_class(selected_reads_file: str, expected_result_requence: str, min_occ: int) -> (float, float):
    """
    apply the kmer consensus algorithm on a defined number of reads and returns the precision and time
    """
        
    consensus_file = "generated_files/consensus_reads.fasta"

    start_time = time.time()
    
    start_primer, stop_primer = expected_result_requence[:20], dfr.reverse_complement(expected_result_requence[-20:])
    
    reads_consensus_class.kmer_consensus(selected_reads_file, consensus_file, start_primer, stop_primer, min_occ)
    
    stop_time = time.time() - start_time

    # read result to get accuracy
    precision = get_precision(expected_result_requence, consensus_file)

    return precision, stop_time


def test_minia_consensus(selected_reads_file: str, expected_result_requence: str) -> (float, float):
    """
    apply the minia consensus algorithm on a defined number of reads and returns the precision and time
    """
    consensus_name = "generated_files/consensus_minia"
    
    minia_command = 'minia/build/bin/minia -in '+ selected_reads_file +' -out '+ consensus_name +' -verbose 0 -kmer-size 25'

    start_time = time.time()

    subprocess.call('/bin/bash -c "$CALLMINIA"', shell=True, env={'CALLMINIA': minia_command})
    
    stop_time = time.time() - start_time

    consensus_file = consensus_name+".contigs.fa"
    
    # read result to get accuracy
    precision = get_precision(expected_result_requence, consensus_file, aligned=False)
        
    return precision, stop_time


def test_MindTheGap_consensus(selected_reads_file: str, expected_result_requence: str) -> (float, float):
    """
    apply Mind The Gap algorithm
    """
    consensus_name = "generated_files/consensus_MindTheGap"
    
    contig_file = "generated_files/mtg_contig_file.fasta"
    
    # mtg needs that start and stop sequences (100 bases) of the result to fill the gap
    dfr.save_dict_to_fasta({"start": expected_result_requence[:100], "stop": expected_result_requence[-100:]}, contig_file)
    
    mtg_command = 'MindTheGap/build/bin/MindTheGap fill -in '+ selected_reads_file +' -out '+ consensus_name +' -verbose 0 -contig '+contig_file+' -max-length '+str(len(expected_result_requence))

    start_time = time.time()

    subprocess.call('/bin/bash -c "$CALLMTG"', shell=True, env={'CALLMTG': mtg_command})

    stop_time = time.time() - start_time
    
    consensus_file = consensus_name + ".insertions.fasta"
    # read result to get accuracy
    precision = get_precision(expected_result_requence[69:-69], consensus_file, aligned=False)
        
    return precision, stop_time


def save_results(n_read_array: list, precision_array: list, time_array: list, script_used: str, save=True) -> None:
    """
    plot or save the figure for precision and computing time of the kmer_consenus algorithm
    """
    
    time_file = "results_graphs/"+script_used+"_time"
    precision_file = "results_graphs/"+script_used+"_precision"
    
    plt.figure(1)
    plt.plot(n_read_array, precision_array, color='blue')
    
    plt.xlabel("n_reads")
    plt.ylabel("precision (%)")

    plt.xlim(left=0)
    plt.ylim(0,110)
    
    if save:
        plt.savefig(precision_file+".png")
        plt.clf() # reset figure
        
        output_file = open(precision_file+".txt","w")
        output_file.write(str(n_read_array)+"\n")
        output_file.write(str(precision_array)+"\n")
        output_file.close()
    
    plt.figure(2)
    plt.plot(n_read_array, time_array, color='red')
    
    plt.xlabel("n_reads")
    plt.ylabel("time (s)")
    
    plt.xlim(left=0)
    plt.ylim(bottom=0)
    
    if save:
        plt.savefig(time_file+".png")
        
        output_file = open(time_file+".txt","w")
        output_file.write(str(n_read_array)+"\n")
        output_file.write(str(time_array)+"\n")
        output_file.close()
    
    if not save:
        plt.show()

    plt.clf() # reset figure


def get_true_expected_sequence(fragments_path: str) -> str:
    """
    return the sequence expected from the consensus
    different of the source sequence because some banned word have been corrected
    add the forward fragments and remove the primers
    """
    fragments_dict = dfr.read_fasta(fragments_path)
    total_sequence = ""
    for i in range(len(fragments_dict)//2):

        total_sequence += fragments_dict[str(i)+"_Fw"]
        
    return total_sequence[20:-20]
    

def test_multiple_consensus(fastq_path: str, reference_path: str, n_reads_max: int, script_used: str) -> None:

    n_read_array = []
    precision_array = []
    time_array = []
    
    #fastq_path = container_path + "/0/7_sequenced_reads.fastq"

    _, expected_result_requence = dfr.read_single_sequence_fasta(reference_path) #get_true_expected_sequence(container_path + "/0/2_fragments.fasta")
    
    repetitions = 10

    for i in range(1, 51):
        n_reads = int(float(i/50)*n_reads_max)
        print("___Consensus at",str(n_reads),"reads___")
        precision = 0
        consensus_time = 0
        for j in range(repetitions):          
            
            selected_reads_file = "generated_files/read_"+str(n_reads)+".fastq"
            
            # generate a sub read files
            get_random_reads(fastq_path, selected_reads_file, n_reads)
            
            min_occ = 4
            
            # apply the consensus and return precisions and time
            if "class" in script_used:
                precision_j, time_j = test_reads_consensus_class(selected_reads_file, expected_result_requence, min_occ)
            else:
                precision_j, time_j = test_reads_consensus(selected_reads_file, expected_result_requence, min_occ)
                    
            precision += precision_j
            consensus_time += time_j
            
            # delete generated read file
            if os.path.exists(selected_reads_file): os.remove(selected_reads_file)

        # save the means in lists
        n_read_array.append(n_reads)
        precision_array.append(round(precision/repetitions,2))
        time_array.append(round(consensus_time/repetitions,2))
        
        
        # save the data each loop in case of script death
        save_results(n_read_array, precision_array, time_array, script_used)


if __name__ == "__main__":
    
    # --------- argument part -------------#
    parser = argparse.ArgumentParser(description='test kmer consensus')
    parser.add_argument('-i', action='store', dest='reads_path', required=True,
                        help='path to the fastq of reads')
    parser.add_argument('-r', action='store', dest='reference_path', required=True,
                        help='path to the fasta of reference sequence')
    parser.add_argument('-m', action='store', dest='n_reads_max', required=True,
                        type=int, help='maximum number of reads')
    parser.add_argument('--script', action='store', dest='script', required=True,
                        type=str, help='consensus script to test')
    arg = parser.parse_args()
        
    test_multiple_consensus(arg.reads_path, arg.reference_path,arg.n_reads_max, arg.script)
    
    
    
    

