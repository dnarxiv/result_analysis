#!/bin/bash


echo "______________ analysis fragments from all barecodes ______________"

data_dirname=udhr_13jan25/FLG01
fragments_filename=udhr_all_payloads


if false; then # quick paragraph comment, change to true/false to activate/deactivate this part

# convert the blast output to an extraction file id\seq\listFrag\...
for read_file_path in 0_data/"$data_dirname"/* ; do

	read_file_name="${read_file_path##*/}" && read_file_name="${read_file_name%.*}"

	blast_file=1_results/$data_dirname/blast_"$read_file_name".blast

	extraction_file=1_results/$data_dirname/extraction_"$read_file_name".txt

	echo extraction of "$blast_file"

	python3 blast_data_management.py $read_file_path $blast_file $extraction_file

done

fi

mkdir -p 2_analysis/$data_dirname

# analysis of the extraction
for read_file_path in 0_data/"$data_dirname"/* ; do

	read_file_name="${read_file_path##*/}" && read_file_name="${read_file_name%.*}"

	extraction_file=1_results/$data_dirname/extraction_"$read_file_name".txt
  analysis_file=2_analysis/$data_dirname/analysis_"$read_file_name".txt

	rm -f $analysis_file

	python3 fragments_analysis.py -i "$extraction_file" -f 0_data/fragments/"$fragments_filename".fasta -o "$analysis_file"

done

#-------------- Exit --------------#
echo "___Fin du processus !___"

exit 0

# scp -r oboulle@dnarxiv.irisa.fr:~/Documents/result_analysis/fragment_assembly_analysis/results/demultiplexed_v2 ~/Documents/result_analysis/fragment_assembly_analysis/results
