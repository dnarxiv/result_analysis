import sys
import os
import inspect
import ast

currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0, os.path.dirname(parentdir)+"/synthesis_modules")
import dna_file_reader as dfr


class Fragment:
    
    junction_fw = [] # forward junction of the fragments
    junction_rv = [] # reverse junction
    frag_dict = {}
    
    def init_junctions(fragments_dict):
        """
        init the class constants of junctions
        """
        #Fragment.junction_fw = list(key for key in fragments_dict.keys() if "_Fw" in key)
        #Fragment.junction_rv = list(key for key in fragments_dict.keys() if "_Rvc" in key)[::-1] # -> the junction is reversed
        Fragment.frag_dict = fragments_dict

        # TODO for oligo
        """
        for i in range(1,11):
            Fragment.junction_fw += ["A"+str(i)+"_Fw"]+[k +"_Fw" for k in "BCDEFGHI"]+["J"+str(i)+"_Fw"] 
            Fragment.junction_rv += ["J"+str(11-i)+"_Rv"]+[k +"_Rv" for k in "IHGFEDCB"]+["A"+str(11-i)+"_Rvc"]
        """

        # TODO for semi ordered
        #Fragment.junction_fw = ["Oligo_"+ k +"_Fw" for k in "ABCDEFGHIJ"]
        #Fragment.junction_rv = ["Oligo_"+ k +"_Rvc" for k in "JIHGFEDCBA"]

        # TODO for udhr
        for i in range(1,13):
            Fragment.junction_fw += ["mol_"+str(i)+"_eBlock_" + str(k) +"_Fw" for k in range(1,9)]
            Fragment.junction_rv += ["mol_"+str(i)+"_eBlock_" + str(k) +"_Rvc" for k in range(8,0,-1)]


    def __init__(self, fragment_array: list) -> None:
        """
        init the fragment from an array ('name', score, [start, stop])
        """
        self.name = fragment_array[0]
        self.score = fragment_array[1]
        self.start_position = fragment_array[2][0]
        self.stop_position = fragment_array[2][1]
        
        self.length = self.stop_position - self.start_position
        self.has_5p = False#fragment_array[2] == "5p"
        self.has_3p = False#fragment_array[3] == "3p"
        

    def add_data(self, preceding_fragment, following_fragment) -> None:
        """
        add information about following and preceding fragments
        """
        self.is_first = preceding_fragment.name == "First"
        self.is_last = following_fragment.name == "Last"
        self.is_good_preceder = self.get_correct_follower() == following_fragment.name
        self.is_good_follower = self.get_correct_preceder() == preceding_fragment.name
        self.follower = following_fragment
        self.preceder = preceding_fragment
        

    def get_correct_preceder(self) -> str:
        """
        return the name of the correct preceding fragment in the junction
        """
        if self.name in self.junction_fw:
            index_frag = self.junction_fw.index(self.name)
            return (["First"] + self.junction_fw + ["Last"])[index_frag-1]
        if self.name in self.junction_rv:
            index_frag = self.junction_rv.index(self.name)
            return (["First"] + self.junction_rv + ["Last"])[index_frag-1]
        #print("get_preceder : error unknown fragment", self.name)


    def get_correct_follower(self) -> str:
        """
        return the name of the correct following fragment in the junction
        """
        if self.name in self.junction_fw:
            index_frag = self.junction_fw.index(self.name)
            return (["First"] + self.junction_fw + ["Last"])[index_frag+1]
        if self.name in self.junction_rv:
            index_frag = self.junction_rv.index(self.name)
            return (["First"] + self.junction_rv + ["Last"])[index_frag+1]
        #print("get_follower : error unknown fragment", self.name)
    
    
    def is_fw(self) -> bool:
        """true if fragment is the the forward junction"""
        return "_Fw" in self.name
    
    
    def is_rv(self) -> bool:
        """true if fragment is the the reverse junction"""
        return "_Rv" in self.name
    
        
    def short_name(self) -> str:
        """name of the fragment without _Fw/_Rv suffix"""
        return short_fragment_name(self.name)
        
        
    def __str__(self) -> str:
        """
        print(fragment)
        """
        fragment_print = "("+self.name+", ["+str(self.start_position)+","+str(self.stop_position)+"], "+("5p" if self.has_5p else "_")+", "+("3p" if self.has_3p else "_")+", "+str(self.score)+")"
        return fragment_print
    
    
    def __repr__(self):
        """
        when printing a list of fragments
        """
        return self.name


def init_fragment_list(fragment_list_string: str) -> list:
    """
    from a line of an extracted fragments file, return the list of initialized Fragments objects 
    """
    fragment_list = []
    fragment_array_list = ast.literal_eval(fragment_list_string)
    for fragment_array in fragment_array_list:
        fragment_list.append(Fragment(fragment_array))
    
    for i in range(1, len(fragment_list)-1):
        fragment = fragment_list[i]
        fragment.add_data(fragment_list[i-1], fragment_list[i+1])
    
    return fragment_list


def unrecognized_fragment(start_position: int, stop_position: int) -> Fragment:
    """ init an unrecognized fragment """
    fragment_array = ['Unrecognized', 0, [start_position, stop_position]]
    return Fragment(fragment_array)
    
    
def short_fragment_name(name: str) -> str:
    # name of the fragment without _Fw or _Rvc
    if name == "Unrecognized":
        return name
    #return name.split("_")[0] #TODO
    #cut = name.split("_")
    #return cut[-2]
    #name = name.replace("_1", "").replace("_2","") # to merge splited fragments
    name = name.replace("_bis", "")
    name = name.replace("_Fw", "").replace("_Rvc", "")
    return name


def get_junction_short_names(bigBlock_num):
    """
    return the list of short names of the forward junction
    """
    junction = list([key for key in Fragment.frag_dict.keys() if "mol_"+str(bigBlock_num)+"_" in key])

    return list(dict.fromkeys([short_fragment_name(name) for name in junction])) # remove duplicate when split fragments


def get_original_fragment_size(fragments_dict):
    """
    return the length of fragments in the assembly
    """
    if len(fragments_dict) > 2:
        some_fragment = list(fragments_dict.values())[2] # first fragment and its reverse can be shorter than every others, so the second is picked
    else:
        some_fragment = list(fragments_dict.values())[0]
    return len(some_fragment)


