#!/bin/bash



echo "______________ extract fragments from all barecodes ______________"

data_dirname=udhr_13jan25/FLG01
fragments_filename=udhr_all_payloads


mkdir -p blast_db/$fragments_filename
blast_db_path=blast_db/db_"$fragments_filename"
mkdir -p 1_results/$data_dirname

# init blast database with the fragments
/home/dlavenier/ncbi-blast-2.13.0+/bin/makeblastdb -dbtype nucl -in 0_data/fragments/"$fragments_filename".fasta -parse_seqids -out "$blast_db_path"/references_db

for read_file_path in 0_data/"$data_dirname"/* ; do

  read_file_name="${read_file_path##*/}" && read_file_name="${read_file_name%.*}"

	echo blast on "$read_file_name"

	result_file=1_results/$data_dirname/blast_$read_file_name.blast

  # run blast
	/home/dlavenier/ncbi-blast-2.13.0+/bin/blastn -query $read_file_path -db "$blast_db_path"/references_db -outfmt 6 -out $result_file

done


#-------------- Exit --------------#
echo "___Fin du processus \!___"

exit 0

# scp -r oboulle@dnarxiv.irisa.fr:~/Documents/result_analysis/fragment_assembly_analysis/results/*.txt ~/Documents/result_analysis/fragment_assembly_analysis/results/
