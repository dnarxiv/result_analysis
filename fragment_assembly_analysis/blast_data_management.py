import sys
import os
import matplotlib.pyplot as plt


def blast_output_to_concatemer_counter(blast_txt_path, output_path):
    """
    convert a blast output txt to a txt counting the number of concatened sequences per read
    """

    if not ".txt" in blast_txt_path or not ".txt" in output_path:
        print("bad inputs for txt_to_fasta : "+blast_txt_path+" to "+output_path)
        exit(0)

    ff = open(blast_txt_path)
    fo = open(output_path,"w")
    new_line = ff.readline()
    current_seq = ""
    current_count = 0 # length of the concatemer
    while new_line != "":
        split_line = new_line.split("\t")
        new_line_seq = split_line[0]
        if new_line_seq == current_seq: # increment the count for the seq
            current_count += 1
        else:
            if current_count > 0: # ignore init value
                fo.write(current_seq+" "+split_line[1]+" "+str(current_count)+"\n") # write seq name
            current_seq = new_line_seq
            current_count = 1

        new_line = ff.readline()
    ff.close()
    fo.close()


def get_ref_presence(input_blast, output_txt):
    """
    take a blast output, print the number of occurrences for each reference
    names of refs must be SeqX format
    """
    all_precision_sum = 0
    perfect_count = 0 # count number of 100 precision
    total_seq = 0
    ref_list = [0 for _ in range(255)]
    precision_sumlist = [0 for _ in range(255)]
    ref_perfect = [0 for _ in range(255)]

    with open(input_blast, 'r') as input, open(output_txt, 'w') as output:
        line = input.readline()
        while line != "":
            total_seq += 1
            line_ref_num = int(line.split('\t')[1].replace("Seq",""))
            line_precision = float(line.split('\t')[2])
            all_precision_sum += line_precision
            if line_precision == 100:
                perfect_count += 1
                ref_perfect[line_ref_num-1] += 1

            ref_list[line_ref_num-1] += 1
            precision_sumlist[line_ref_num-1] += line_precision

            line = input.readline()

        for ref_num in range(255):
            num_occ = ref_list[ref_num-1]
            if num_occ > 0:
                output.write("Seq"+str(ref_num)+'\t'+str(num_occ)+'\t'+str(precision_sumlist[ref_num-1]/num_occ)+'\n')
            else:
                output.write("Seq"+str(ref_num)+'\t0\t0\n')
    print(str(round(all_precision_sum/total_seq,2))+ "% precision, "+ str(round(100*perfect_count/total_seq,2))+"% are perfect")
    #print(ref_perfect)


def convert_blast_output_to_analysis(reads_fasta, results_blast, analysis_file):
    """
    use a blast txt file and a read fasta to explicit the content of each line
    """
    with open(results_blast, 'r') as input_blast, open(reads_fasta, 'r') as input_reads, open(analysis_file, 'w') as output:
        line = input_reads.readline()

        current_blast_line = input_blast.readline().split("\t")
        current_blast_id = current_blast_line[0]

        while line != "":
            seq_id = line.replace("\n","").replace(">","")
            seq_read = input_reads.readline()
            seq_content_list = []
            while seq_id == current_blast_id:

                if int(current_blast_line[8]) <= int(current_blast_line[9]): # direction of fragment depends on start pos and end pos of ref in read
                    fragment_name = current_blast_line[1]+"_Fw"
                else:
                    fragment_name = current_blast_line[1]+"_Rvc"
                fragment_score = round(float(current_blast_line[2])/10,1)
                fragment_start_pos = int(current_blast_line[6])-1
                fragment_stop_pos = int(current_blast_line[7])
                seq_content_list.append([fragment_name, fragment_score, [fragment_start_pos, fragment_stop_pos]])

                current_blast_line = input_blast.readline().split("\t")
                current_blast_id = current_blast_line[0]

            if len(seq_content_list) > 0: # TODO ignore reads wo mols
                seq_content_list.sort(key=lambda x: x[2][0]) # sort the fragments by the start position
                output.write("@"+seq_id+"\n"+seq_read)
                output_content = [('First', 10, [0, 0])]+seq_content_list+[('Last', 10, [len(seq_read), len(seq_read)])]
                output.write(str(output_content)+"\n\n")

            line = input_reads.readline()

            
def divide_blasted_sequences(input_fasta, input_blast, output_fasta):
    """
    give reads file and blast output file to divide all the reads in : aligned in the blast/not aligned in the blast
    """

    if not ".fasta" in input_fasta:# or not ".fasta" in output_fasta:
        print("bad inputs for extract_blasted_sequences : "+input_fasta+" "+output_fasta)
        exit(0)

    blasted_lines_list = []
    last_added_line = ""

    # parse blast output to get list of seq id mentionned
    ff = open(input_blast)
    new_line = ff.readline()
    while new_line != "":
        seq_id = new_line.split("\t")[0]
        ref_id = new_line.split("\t")[1]
        if seq_id != last_added_line:
            blasted_lines_list.append([seq_id, ref_id])
            last_added_line = seq_id
        new_line = ff.readline()
    ff.close()
    print("blast_output parsed...")

    with open(input_fasta, 'r') as f_in_reads:
        reads_lines = f_in_reads.readlines()

    reads_index_min = 0

    print(len(blasted_lines_list))
    print(len(reads_lines)/2)

    #f_out = open(output_fasta, 'w')

    for seq_id, ref_id in blasted_lines_list:
        not_found = True
        reads_index = reads_index_min
        while not_found and reads_index < len(reads_lines): # parse the fasta until finding seq_id
            id_line = reads_lines[reads_index]
            seq = reads_lines[reads_index+1]
            reads_index += 2

            if id_line.replace(">","").replace("\n","") == seq_id:
                not_found = False
                f_out = open(output_fasta+"/"+ref_id+".fasta", 'a') #TODO
                f_out.write(id_line)
                f_out.write(seq) # write sequence in aligned file
                f_out.close() #TODO

                #print(seq_id+" at "+str(reads_index))
                reads_index_min = reads_index

            else:
                #f_out.write(new_line)
                #f_out.write(seq) # write in unaligned file
                continue



    f_in_reads.close()
    #f_out.close()

    print("reads filtered")


def divide_blast_by_mol(input_blast, output_dir):

    mol_lines_dict = {}

    # parse blast output to get list of seq id mentionned
    with open(input_blast, 'r') as ff:
        new_line = ff.readline()
        while new_line != "":
            ref_id = new_line.split("\t")[1]
            mol_num = ref_id.split("_")[1] # names are like mol_7_eBlock_6
            mol_lines_dict[mol_num] = mol_lines_dict.get(mol_num, [])
            mol_lines_dict[mol_num].append(new_line)

            new_line = ff.readline()

    for mol_num, mol_lines in mol_lines_dict.items():
        with open(output_dir+"/mol_"+mol_num+".blast", 'w') as output_file:
            output_file.write("".join(mol_lines))



def filter_blast(blast_file, output_path):
    """
    filter lines by alignment size
    """
    output_lines = []

    with open(blast_file, 'r') as input:

        line = input.readline()
        while line != "":
            alignment_length = int(line.split("\t")[3])
            if alignment_length > 160:
                output_lines.append(line)

            line = input.readline()


    with open(output_path, 'w') as output:
        for line in output_lines:
            output.write(line)


def length_analysis_by_alignment(blast_file):

    length_dict = {}

    with open(blast_file, 'r') as input:

        line = input.readline()
        while line != "":
            alignment_name = line.split("\t")[1]
            alignment_length = int(line.split("\t")[3])

            length_dict[alignment_name] = length_dict.get(alignment_name, [])
            length_dict[alignment_name].append(alignment_length)

            line = input.readline()

    alignment_names = [i+str(n) for i in ["Oligo_"+ k for k in "ABCDEFGHIJ"] for n in range(1, 11)]

    for alignment_name in alignment_names:
        length_list = length_dict[alignment_name]
        average = sum(length_list)/ float(len(length_list))
        length_list.sort()
        median = length_list[len(length_list)//2]
        print(alignment_name+" : average "+str(round(average,2))+" median "+str(median))


def length_distribution(blast_file):
    """
    show the distribution of alignments length
    """
    with open(blast_file, "r") as input:
        lines = input.readlines()

    #length_distribution_dict = {}
    length_distribution = {}

    length_interval = 5 # size of the interval of length columns

    for line in lines:

        alignment_length = int(line.split("\t")[3])
        seq_length = length_interval*(alignment_length//length_interval)

        alignment_name = line.split("\t")[1]

        #length_distribution = length_distribution_dict.get(alignment_name, {})

        length_distribution[seq_length] = length_distribution.get(seq_length, 0) + 1 # +1 (number of read) +seq_length

        #length_distribution_dict[alignment_name] = length_distribution


    alignment_nbr = sum(length_distribution.values()) # number of alignments with this oligo

    ax = plt.gca()
    ax.set_xlim([0, max(length_distribution.keys())+10])
    ax.set_ylim([0, 100])
    plt.bar(list(length_distribution.keys()), [100*k/alignment_nbr for k in length_distribution.values()], length_interval, color='g')

    plt.xlabel("length of sequence")
    plt.ylabel("% of reads")
    plt.title("alignments lengths")

    #plt.savefig('2_analysis/data_oct_24_2/distribution_sizes/'+graph_name)
    plt.show()


def alignments_distribution(blast_file):
    """
    show the distribution of the alignments
    """
    with open(blast_file, "r") as input:
        lines = input.readlines()

    alignment_distribution_dict = {}


    for line in lines:

        alignment_name = line.split("\t")[1]
        alignment_distribution_dict[alignment_name] = alignment_distribution_dict.get(alignment_name, 0) + 1

    alignment_names = [i+str(n) for i in ["Oligo_"+ k for k in "ABCDEFGHIJ"] for n in range(1, 11)]
    for alignment_name in alignment_names:
        print(alignment_name+" "+str(alignment_distribution_dict.get(alignment_name,0)))



intput_path = sys.argv[1]
if len(sys.argv) > 2:
    input2 = sys.argv[2]
if len(sys.argv) > 3:
    input3 =sys.argv[3]

#filter_blast(intput_path, input2)
#length_distribution(intput_path)
#get_ref_presence(intput_path, input2)
#divide_blasted_sequences(intput_path, input2, input3)
#blast_output_to_concatemer_counter(intput_path, input2)

file_name="FLG1-6_all"

blast_name="1_results/udhr_21jan25/blast_"+file_name+".blast"
dir_name = "1_results/udhr_21jan25/"+file_name
divide_blast_by_mol(blast_name, dir_name)

reads_path = "0_data/udhr_21jan25/"+file_name+".fasta"
for i in range(1, 13):

    convert_blast_output_to_analysis(reads_path, dir_name+"/mol_"+str(i)+".blast", dir_name+"/extraction_mol_"+str(i)+".txt")
#alignments_distribution(intput_path)
