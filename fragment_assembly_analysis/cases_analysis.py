import sys
import os
import inspect
import argparse

import analysis_classes as ac
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0, os.path.dirname(parentdir)+"/synthesis_modules")
import dna_file_reader as dfr


possible_cases = ["1", "2", "3", "4", "4'", "5", "5'", "6", "6'", "6''", "7", "7'", "7''", "8", "8'", "8''","Unclassified"]

min_frag_size = 57
min_frag_size_extrems = 53

X_dist = 10
 
    
def get_case_old(fragment: Fragment, preceding_fragment: Fragment, following_fragment: Fragment) -> str:
    
    dist_with_preceder = fragment.start_position - preceding_fragment.stop_position
    dist_with_follower = following_fragment.start_position - fragment.stop_position
    
    correct_size = fragment.length >= min_frag_size
    if method == "Gibson4" and fragment.name in ["A_Rv", "J_Fw"]:
        correct_size = fragment.length > min_frag_size_extrems
    if method == "GoldenGate4" and fragment.name in ["A_Fw", "J_Rv"]:
        correct_size = fragment.length > min_frag_size_extrems

    after_first = fragment.is_first # the fragment is the first of its molecule
    before_last = fragment.is_last # the fragment is the last of its molecule
    
    is_good_follower = fragment.is_good_follower # the fragment follows the N-1 fragment
    is_good_preceder = fragment.is_good_preceder # the fragment is followed by the N+1 fragment
    
    if after_first and before_last \
        and dist_with_preceder < X_dist and dist_with_follower < X_dist:
        if correct_size:
            return "1"
        else:
            return "2"
    if is_good_follower and is_good_preceder:
        if correct_size:
            return "3"
        else:
            return "6''"
    if is_good_follower and before_last and correct_size:
        if dist_with_follower < X_dist:
            return "4"
        else:
            return "5"
    if after_first and is_good_preceder and correct_size:
        if dist_with_preceder < X_dist:
            return "4'"
        else:
            return "5'"
    if after_first and is_good_preceder and not correct_size and dist_with_preceder < X_dist:
        return "6"
    if is_good_follower and before_last and not correct_size and dist_with_follower < X_dist:
            return "6'"
    if after_first and before_last and correct_size:
        if dist_with_preceder < X_dist and dist_with_follower > X_dist:
            return "7"
        if dist_with_preceder > X_dist and dist_with_follower < X_dist:
            return "7'"
        if dist_with_preceder > X_dist and dist_with_follower > X_dist:
            return "7''"
    if not after_first and not is_good_follower \
        and (before_last or is_good_preceder) and dist_with_preceder < X_dist:
        return "8"
    if not before_last and not is_good_preceder \
        and (after_first or is_good_follower) and dist_with_follower < X_dist:
        return "8'"
    if not after_first and not is_good_follower and not before_last and not is_good_preceder \
        and dist_with_preceder < X_dist and dist_with_follower < X_dist:
        return "8''"
    # undefined cases
    #print("9", preceding_fragment, "-", fragment, "-", following_fragment)
    return "Unclassified"


def get_case(fragment: Fragment) -> str:
    
    if fragment.is_good_follower and fragment.is_good_preceder: # N-1 > N > N+1
        return "1"
    if fragment.is_good_follower and not fragment.is_good_preceder: # N-1 > N > ?
        if fragment.has_3p:
            return "2"
        else:
            return "2_no_3p"
    if not fragment.is_good_follower and fragment.is_good_preceder: # ? > N > N+1
        if fragment.has_5p:
            return "3"
        else:
            return "3_no_5p"
    return "4" # ? > N > ?


def get_case_5p_3p(fragment: Fragment) -> str:
    if fragment.has_5p and fragment.has_3p:
        return "5p3p"
    if fragment.has_5p and not fragment.has_3p:
        return "5p"
    if not fragment.has_5p and fragment.has_3p:
        return "3p"
    return "None"


def get_case_position(fragment: Fragment) -> str:
    if fragment.is_first and not fragment.is_last and not fragment.name in ["A_Fw", "J_Rv"]:
        return "3"
    if not fragment.is_first and fragment.is_last:
        return "2"
    return "1"


def liaison_AJ_case(fragment: Fragment) -> str:
    if fragment.name[0] == "J" and following_fragment.name[0] == "A":
        return "1"
    if preceding_fragment.name[0] == "J" and fragment.name[0] == "A":
        return "1"
    return "2"


def get_monofragment_case(fragment: Fragment) -> str:
    
    if fragment.is_first and fragment.is_last:
        return "1"
    return "2"


def get_correct_follower_length(sequence: str, theorical_fragment: str) -> int:
    """
    length of the sequence that match the theorical follower fragment
    """
    max_common_sequence_size = min(len(sequence), len(theorical_fragment))
    for i in range(min(len(sequence), len(theorical_fragment))):
        if sequence[i] != theorical_fragment[i]:
            return i
    return max_common_sequence_size


def get_correct_preceder_length(sequence: str, theorical_fragment: str) -> int:
    """
    length of the sequence that match the theorical preceder fragment
    """
    max_common_sequence_size = min(len(sequence), len(theorical_fragment))
    for i in range(max_common_sequence_size):
        if sequence[-(i+1)] != theorical_fragment[-(i+1)]:
            return i
    return max_common_sequence_size


def analysis(input_path: str, fragments_path: str, score_threshold=None):

    input = open(input_path, "r")
    lines = input.readlines()
    input.close()

    nbr_mol = int(len(lines)/4)
    
    fragments_dict = dfr.read_fasta(fragments_path)
        
    cases_per_frag_dict = {}
    init_dict = {"occurrence" : 0, "mean_size" : 0, "length_before" : 0, "occurrence_region_before" : 0, "length_after" : 0, "occurrence_region_after" : 0, "5p" : 0, "3p" : 0}
    cases_names_list = ["1", "2", "2_no_3p", "3", "3_no_5p", "4"]
    length_before_case_3_dict = {}
    length_after_case_2_dict = {}
    length_cases_dict = {k : {} for k in cases_names_list}
    for case_name in cases_names_list:
        cases_per_frag_dict[case_name] = {}
        for frag_num in range(1, len(ac.total_junction_fw)-1):
            cases_per_frag_dict[case_name][ac.total_junction_fw[frag_num]] = dict(init_dict)
        for frag_num in range(1, len(ac.total_junction_rv)-1):
            cases_per_frag_dict[case_name][ac.total_junction_rv[frag_num]] = dict(init_dict)
    
    cases_per_read = []

    for i in range(nbr_mol):
        mol_sequence =  lines[4*i+1].replace("\n","")
        fragment_list_string = lines[4*i+2].replace("\n","")
        fragment_list = ac.init_fragment_list(fragment_list_string)
        if score_threshold != None:
            #filter fragments with score < threshold
            fragment_list = [f for f in fragment_list if f.score >= score_threshold]
        
        cases_list = []
                
        for j in range(1, len(fragment_list)-1): # all fragments except "First" and "Last"
            fragment = fragment_list[j]

            fragment_case = get_case_position(fragment) # TODO Choose case type  

            cases_list.append(fragment_case)
            
            cases_per_frag_dict[fragment_case][fragment.name]["occurrence"] += 1
            cases_per_frag_dict[fragment_case][fragment.name]["mean_size"] += fragment.length

            if fragment.has_5p:
                cases_per_frag_dict[fragment_case][fragment.name]["5p"] += 1
              
            if fragment.has_3p:
                cases_per_frag_dict[fragment_case][fragment.name]["3p"] += 1
                
            if fragment.name not in ["A_Fw", "J_Rv"] and not fragment.is_good_follower: # ["A_Fw", "A_Rv", "B_Fw", "I_Rv", "J_Rv", "J_Fw"]
                correct_preceder_length = get_correct_preceder_length(mol_sequence[:fragment.start_position], fragments_dict[fragment.get_correct_preceder()])
                cases_per_frag_dict[fragment_case][fragment.name]["length_before"] += correct_preceder_length
                cases_per_frag_dict[fragment_case][fragment.name]["occurrence_region_before"] += 1
                if fragment_case == "3":
                    length_before_case_3_dict[correct_preceder_length] = length_before_case_3_dict.get(correct_preceder_length, 0) +1
            
            if fragment.name not in ["J_Fw", "A_Rv"] and not fragment.is_good_preceder: # ["J_Fw", "I_Fw", "B_Rv", "A_Rv", "A_Fw", "J_Rv"]
                correct_follower_length = get_correct_follower_length(mol_sequence[fragment.stop_position:], fragments_dict[fragment.get_correct_follower()])
                cases_per_frag_dict[fragment_case][fragment.name]["length_after"] += correct_follower_length
                cases_per_frag_dict[fragment_case][fragment.name]["occurrence_region_after"] += 1
                if fragment_case == "2":
                    length_after_case_2_dict[correct_follower_length] = length_after_case_2_dict.get(correct_follower_length, 0) +1

            length_cases_dict[fragment_case][fragment.length] = length_cases_dict[fragment_case].get(fragment.length, 0) +1       
                                
        full_case = []
        full_case.append(lines[4*i].replace("\n","")) # index
        full_case.append(lines[4*i+1].replace("\n","")) # sequence
        full_case.append(fragment_list_string) # fragments list
        full_case.append(cases_list) # cases list
        cases_per_read.append(full_case)
        
    return cases_per_frag_dict, cases_per_read, length_before_case_3_dict, length_after_case_2_dict, length_cases_dict
       

def save_results(cases_per_frag_dict, length_before_case_3_dict, length_after_case_2_dict, length_cases_dict, output_path: str) -> None:
    
    output = open(output_path, "w")
    
    for case_name in cases_per_frag_dict:
        output.write("Case "+case_name+";")
        
        sorted_frag_list = sorted(cases_per_frag_dict[case_name].keys())
        output.write(";".join(sorted_frag_list)+"\n")
        for sub_case in ["occurrence", "mean_size", "length_before", "length_after", "5p", "3p"]:
            sub_case_string = sub_case+";"
            for fragment in sorted_frag_list:
                if sub_case == "mean_size" and cases_per_frag_dict[case_name][fragment]["occurrence"] > 0:
                    sub_case_string += str(round(cases_per_frag_dict[case_name][fragment]["mean_size"]/cases_per_frag_dict[case_name][fragment]["occurrence"], 1))+";"
                elif sub_case == "length_before" and cases_per_frag_dict[case_name][fragment]["occurrence_region_before"] > 0:
                    sub_case_string += str(round(cases_per_frag_dict[case_name][fragment]["length_before"]/cases_per_frag_dict[case_name][fragment]["occurrence_region_before"], 1))+";"
                elif sub_case == "length_after" and cases_per_frag_dict[case_name][fragment]["occurrence_region_after"] > 0:
                    sub_case_string += str(round(cases_per_frag_dict[case_name][fragment]["length_after"]/cases_per_frag_dict[case_name][fragment]["occurrence_region_after"], 1))+";"
                else:
                    sub_case_string += str(cases_per_frag_dict[case_name][fragment][sub_case])+";"
            output.write(sub_case_string+"\n")
        output.write("\n")
    """
    output.write("length before;")
    max_length_before = max(length_before_case_3_dict.keys())
    for i in range(max_length_before+1):
        output.write(str(i)+";")
    output.write("\nCas 3;")
    for i in range(max_length_before+1):
        output.write(str(length_before_case_3_dict.get(i, 0))+";")
    """
    output.write("\nlength fragment;")
    min_length_fragment = 40
    max_length_fragment = 63
    for i in range(min_length_fragment, max_length_fragment+1):
        output.write(str(i)+";")
    for case_name in length_cases_dict:
        output.write("\n"+case_name+";")
        for i in range(min_length_fragment, max_length_fragment+1):
            output.write(str(length_cases_dict[case_name].get(i, 0))+";")
   
    """
    output.write("\nlength after;")
    max_length_after = max(length_after_case_2_dict.keys())
    for i in range(max_length_after+1):
        output.write(str(i)+";")

    output.write("\nCas 2;")
    for i in range(max_length_after+1):
        output.write(str(length_after_case_2_dict.get(i, 0))+";")
    """
    output.close()
    
    
def print_reads_cases(cases_per_read: list, output_path: str, specific_case=None) -> None:
    """
    optional specific case to only save molecules containing this case
    """
    
    output = open(output_path, "w")
    
    for full_case in cases_per_read:
        
        identifier = full_case[0]
        sequence_read = full_case[1]
        fragment_list_string = full_case[2]
        cases_list = full_case[3]
        
        if specific_case and not specific_case in cases_list:
            continue
        
        output.write(identifier+"\n")
        output.write(sequence_read+"\n")
        output.write(fragment_list_string+"\n")
        if not specific_case:
            output.write(", ".join(cases_list)+"\n")
        output.write("\n")

    output.close()
    print()
    
# =================== main ======================= #
if __name__ == '__main__':
    
    parser = argparse.ArgumentParser(description='analysis of fragment cases')
    parser.add_argument('-i', action='store', dest='input_path', required=True,
                        help='file to read the fragments')
    parser.add_argument('-f', action='store', dest='fragments_fasta_path', required=True,
                        help='file containing the fragments')
    parser.add_argument('-o', action='store', dest='output_path', required=True,
                        help='file to save the result')
    parser.add_argument('-c', action='store', dest='specific_case', required=False,
                        default=None, help='optional : only save molecules containing this case')
    parser.add_argument('-s', action='store', dest='score_threshold', required=False,
                        type=int, default=7, help='minimum alignment score to recognize a fragment')

    arg = parser.parse_args()
    
    method = os.path.basename(arg.input_path).split("_")[0]
    cases_per_frag_dict, cases_per_read, length_before_dict, length_after_dict, length_cases_dict = analysis(arg.input_path, arg.fragments_fasta_path, arg.score_threshold)
    save_results(cases_per_frag_dict, length_before_dict, length_after_dict, length_cases_dict, arg.output_path)
    #print_reads_cases(cases_per_read, arg.output_path, arg.specific_case)

