#!/usr/bin/python3

import os
import sys
import inspect
import time
import argparse
import graphviz
import matplotlib.pyplot as plt


currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0, os.path.dirname(parentdir)+"/sequencing_modules")
sys.path.insert(0, os.path.dirname(parentdir)+"/synthesis_modules")

import dna_file_reader as dfr

import kmer_counting_dsk as dsk


def print_graph(fastq_path):

    kmer_size = 20
    min_occ = 2
    kmer_occurrences_dict = dsk.count_kmers(fastq_path, kmer_size, min_occ)


    #gv_graph = graphviz.Digraph(comment='path_graph')
    #gv_graph.attr(ranksep='2.0', rankdir='LR') # ranksep to increase the distance between nodes, rankdir LR to force left to right

    start_sequence = "TAGCTCCGCGGGTTAATACA"
    stop_sequence =  "TACAACACAGGTCCACGCTA"


    kmer_distribution_dict = {} # key : distance to start, value = occ
    base_distribution_dict = {k:0 for k in range(700)}

    #print(kmer_occurrences_dict.items())

    current_kmer = start_sequence[:kmer_size]

    # add node for start primer
    #gv_graph.node(current_kmer, "Start_Primer_Fw", peripheries='2')
    #gv_graph.node(stop_sequence, "Stop_Primer_Rv", peripheries='2')

    count = 0

    base_distribution_dict[count] = kmer_occurrences_dict[current_kmer]
    kmer_distribution_dict[count] = kmer_occurrences_dict[current_kmer]

    while count < 600:

        best_kmer, best_occ = "", 0
        for base in ["A","C","G","T"]:
            next_kmer = current_kmer[1:]+base

            next_kmer_occ = kmer_occurrences_dict.get(next_kmer, 0)
            if next_kmer_occ > best_occ:
                best_kmer = next_kmer
                best_occ = next_kmer_occ

        if best_occ == 0:
            print("dead end")
            exit(1)

        #print(count, best_kmer,best_occ )

        # end condition = reached end primer
        if best_kmer == stop_sequence[-kmer_size:]:
            #gv_graph.node(best_kmer, "Stop_Primer_Rv", peripheries='2')
            #gv_graph.edge(current_kmer, best_kmer, weight=str(best_occ), label=str(best_occ))
            print("reached end")
            break

        #gv_graph.node(best_kmer, best_kmer[:5])
        #gv_graph.edge(current_kmer, best_kmer, weight=str(best_occ), label=str(best_occ))

        current_kmer = best_kmer
        count += 1

        kmer_distribution_dict[count] = best_occ
        for index, base in enumerate(best_kmer):
            base_distribution_dict[count+index] = max(base_distribution_dict[count+index], best_occ)



    # print graph
    #print(gv_graph.source)
    print(base_distribution_dict)
    name = fastq_path.split("/")[-1].replace(".fastq","")
    #gv_graph.render('deBruijn_'+name, view=True) # save the graph and display it
    distribution(base_distribution_dict, fastq_path)


def distribution(kmer_distribution_dict, fastq_path):

    total_reads = len(dfr.read_fastq(fastq_path))

    ax = plt.gca()
    ax.set_xlim([-10, 240])
    ax.set_ylim([0, 100])#1.1*max(kmer_distribution_dict.values())])

    #eBlocks_colors = ['g']*20 + ['y']*6 + ["gray"]*1 + ['orange']*4 + ["gray"]*432 \
    #                 + ['orange']*4 + ["gray"]*1 + ['y']*6 + ['g']*20
    eBlocks_colors = ['g']*20 + ["gray"]*190 + ['g']*20



    plt.bar(list(kmer_distribution_dict.keys()), [100*k/total_reads for k in kmer_distribution_dict.values()], 1, color=eBlocks_colors)

    #plt.title("barcode"+input_path.split("/")[-1].replace(".txt", "").replace("extraction_",""))

    plt.xlabel("nucleotides of the eBlock")
    plt.ylabel("% of occurrences")

    graph_name = fastq_path.split("/")[-1].replace(".fastq", ".png")
    plt.savefig('2_analysis/data_oct_24_2/distributions_bases/'+graph_name)
    plt.show()


# =================== main =======================#
if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='show the distribution of bases of a fragment')
    parser.add_argument('-i', action='store', dest='input_fastq', required=True,
                        help='reads file')


    # ---------- input list ---------------#
    arg = parser.parse_args()

    print_graph(arg.input_fastq)
