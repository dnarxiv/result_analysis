import sys
import os
import argparse
import inspect
import matplotlib.pyplot as plt

import fragments_identifier as fi
import analysis_classes as ac

currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0, os.path.dirname(parentdir)+"/synthesis_modules")
import dna_file_reader as dfr



blocks_bsaI = { "1": "CAAGACGAAGGTCTCGAACA", "10": "ACGACGAGACCTTCGTCTTG",
               "21": "CAAGACGAAGGTCTCGGCAA", "30": "CACCCGAGACCTTCGTCTTG",
               "31": "CAAGACGAAGGTCTCGCACC", "40": "CCTACGAGACCTTCGTCTTG", 
               "41": "CAAGACGAAGGTCTCGCCTA", "50": "CGGACGAGACCTTCGTCTTG",
               "51": "CAAGACGAAGGTCTCGCGGA", "60": "TGAACGAGACCTTCGTCTTG"}

blocks_overhangs = {"1": "AACA", "10": "ACGA",
                   "21": "GCAA", "30": "CACC",
                   "31": "CACC", "40": "CCTA", 
                   "41": "CCTA", "50": "CGGA",
                   "51": "CGGA", "60": "TGAA"}

blocks_overhangs_false = {"21": "AACA", "30": "ACGA",
                           "31": "GCAA", "40": "CACC",
                           "41": "CACC", "50": "CCTA", 
                           "51": "CCTA", "60": "CGGA",
                           "1": "CGGA", "10": "TGAA"}

def score_alignment(sequence: str, extrem: str) -> (float, list):
    
    if sequence == "": return 0
    
    score, align_coord = fi.get_alignment_score(sequence, extrem)
    return round(score,1) #, align_coord


def eBlocks_analysis(eBlocks_file):
    
    input = open(eBlocks_file, "r")
    lines = input.readlines()
    input.close()
    
    nbr_mol = int(len(lines)/4)
        
    start_extrem_score_distribution = {}
    stop_extrem_score_distribution =  {}

    used_extrems = blocks_overhangs # dict of extremums to use
    
    overhang_presence_dict = {"None": 0, "Begin" : 0, "End": 0, "Both": 0}
    
    for i in range(nbr_mol):
        
        sequence = lines[4*i+1].replace("\n","")
        fragment_list_string = lines[4*i+2].replace("\n","")
        fragment_list = ac.init_fragment_list(fragment_list_string) # get the list of fragments objects from the string list of the results file
        
        ov_begin, ov_end = False, False
        
        for fragment in fragment_list:
            if fragment.name in ["First", "Last"]: # ignore fictional fragments
                continue
            
            fragment_number = fragment.name.split("_")[1]
            
            if fragment_number in used_extrems.keys(): 
                
                if fragment_number.endswith("1"): # first fragment of a Block
                    compared_sequence = used_extrems[fragment_number]
                    extract_zone_start = ""
                    if "_Fw" in fragment.name:
                        begin_zone = max(0, fragment.start_position - 26)
                        extract_zone_start = sequence[begin_zone:begin_zone+8]
                    else: # "_Rv" in fragment.name
                        end_zone = min(len(sequence), fragment.stop_position + 26)
                        extract_zone_start = dfr.reverse_complement(sequence[end_zone-8:end_zone])
                    
                    start_extrem_score = score_alignment(extract_zone_start, compared_sequence)
                    start_extrem_score_distribution[fragment_number] = start_extrem_score_distribution.get(fragment_number, {})
                    start_extrem_score_distribution[fragment_number][start_extrem_score] = start_extrem_score_distribution[fragment_number].get(start_extrem_score, 0) +1
                    ov_begin = (start_extrem_score == 10)
                    
                if fragment_number.endswith("0"): # last fragment of a Block
                    compared_sequence = used_extrems[fragment_number]
                    extract_zone_stop = ""
                    if "_Fw" in fragment.name:
                        end_zone = min(len(sequence), fragment.stop_position + 26)
                        extract_zone_stop = sequence[end_zone-8:end_zone]
                    else: # "_Rv" in fragment.name
                        begin_zone = max(0, fragment.start_position - 26)
                        extract_zone_stop = dfr.reverse_complement(sequence[begin_zone:begin_zone+8])

                    stop_extrem_score = score_alignment(extract_zone_stop, compared_sequence)
                    stop_extrem_score_distribution[fragment_number] = stop_extrem_score_distribution.get(fragment_number, {})
                    stop_extrem_score_distribution[fragment_number][stop_extrem_score] = stop_extrem_score_distribution[fragment_number].get(stop_extrem_score, 0) +1

                    ov_end = (stop_extrem_score == 10)

        if ov_begin and ov_end:
            overhang_presence_dict["Both"] += 1
        if ov_begin and not ov_end:
            overhang_presence_dict["Begin"] += 1
        if not ov_begin and ov_end:
            overhang_presence_dict["End"] += 1
        if not ov_begin and not ov_end:
            overhang_presence_dict["None"] += 1
    print_raw(start_extrem_score_distribution, stop_extrem_score_distribution, "test.txt")
    """for block in start_extrem_score_distribution.keys():
        print("Block "+block)
        print(start_extrem_score_distribution[block][10], sum(start_extrem_score_distribution[block].values()))
        
    for block in stop_extrem_score_distribution.keys():
        print("Block "+block)
        print(stop_extrem_score_distribution[block][10], sum(stop_extrem_score_distribution[block].values()))
    """    
    #print(start_extrem_score_distribution)
    #print_multiple_graphs("start_extrem_overhang_", start_extrem_score_distribution)
    #print(stop_extrem_score_distribution)
    #print_multiple_graphs("stop_extrem_overhang_", stop_extrem_score_distribution)


def print_hist(distribution):
    
    for i, key in enumerate(distribution.keys()):
        plt.figure(i)
        plt.title(key)
        ax = plt.gca()
        ax.set_xlim([0,800])
        ax.set_ylim([0, max(distribution[key].values())+20])
        plt.bar(list(distribution[key].keys()), distribution[key].values(), color='g')

    plt.show()


def print_multiple_graphs(label, distributions_dict):
    
    for i, key in enumerate(distributions_dict.keys()):
        #plt.figure(i)
        print_graph(label + key, distributions_dict[key])


def print_graph(graph_label, distribution):
    plt.title(graph_label)
    plt.plot(*zip(*sorted(distribution.items())), marker='o')
    plt.savefig(graph_label+".png")
    #plt.clf()
    plt.show()
    
    
def print_raw(start_extrem_score_distribution, stop_extrem_score_distribution, output_path):
    
    output = open(output_path, "w")
    
    output.write("overhang start\n")
    for block in start_extrem_score_distribution.keys():
        output.write("Block "+block+" scores :\n")
        for key in sorted(start_extrem_score_distribution[block]):
            output.write(str(key)+" : "+ str(start_extrem_score_distribution[block][key])+" | ")
        output.write("\n")
    output.write("\n")   
    
    output.write("overhang stop\n")
    for block in stop_extrem_score_distribution.keys():
        output.write("Block "+block+" scores :\n")
        for key in sorted(stop_extrem_score_distribution[block]):
            output.write(str(key)+" : "+ str(stop_extrem_score_distribution[block][key])+" | ")
        output.write("\n")    
    
    output.close()
    
# =================== main ======================= #
if __name__ == '__main__':
    
    parser = argparse.ArgumentParser(description='analysis for eblocks')
    parser.add_argument('-i', action='store', dest='eBlocks_file', required=True,
                        help='txt file of eBlocks extraction')

    arg = parser.parse_args()
    
    eBlocks_analysis(arg.eBlocks_file)
    


