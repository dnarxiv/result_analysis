import sys
import os
import ast
import argparse
import matplotlib.pyplot as plt
import analysis_classes as ac
import inspect

currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0, os.path.dirname(parentdir)+"/synthesis_modules")
import dna_file_reader as dfr


def increment_dict(key: str, dict: dict) -> None:
    """
    increment the counter for this key in the dict, or initialize it at 1 when the key is new
    """
    if key in dict:
        dict[key] += 1
    else:
        dict[key] = 1


def append_dict(key: str, value: str, dict: dict) -> None:
    """
    add a value to the list for this key in the dict, or initialize it at [value] when the key is new
    """
    if key in dict:
        dict[key].append(value)
    else:
        dict[key] = [value]


def get_nbr_value_dict(key: str, dict: dict) -> int:
    """
    return the number associated to the key, or 0 if the key is not in the dictionary
    """
    if dict is None:
        return 0
    if key in dict:
        return dict[key]
    else:
        return 0


def is_subjunction(fragment_list: list, junction: list) -> bool:
    """
    true if the list of fragments is included in the same order in the junction
    """

    if len(fragment_list) == 0:
        return False

    if "".join(fragment_list) in "".join(junction):
        return True
    else:
        return False


def is_possible_subjunction(fragment_list: list, junction: list) -> bool:
    """
    when 1 or less unrecognized fragments, true if it could be a correct subjunction when replacing it by an other fragment
    allows A?CD ABC? ?HIJ, but not AB?CD, ?ABC, HIJ?, ABC??, ?B?C
        
    """
    if fragment_list.count("Unrecognized") == 0:
        return is_subjunction(fragment_list, junction)
    
    if len(fragment_list) < 2 or fragment_list.count("Unrecognized") > 1:
        return False
    
    # cases where the list contains one Unrecognized fragment
    if fragment_list[0] == "Unrecognized":
        return is_subjunction(fragment_list[1:], junction[1:]) # ?ABC not allowed
    
    if fragment_list[-1] == "Unrecognized":
        return is_subjunction(fragment_list[:-1], junction[:-1]) # HIJ? not allowed

    if fragment_list[0] not in junction:
            return False
        
    index_of_start = junction.index(fragment_list[0])

    index_of_unknown = fragment_list.index("Unrecognized")

    if index_of_start + index_of_unknown >= len(junction):
        return False
    
    fragment_list[index_of_unknown] = junction[index_of_start + index_of_unknown] # replace the unrecognized by the supposed correct base

    return is_subjunction(fragment_list, junction)


def largest_possible_subjunction(fragment_list: list, junction: list) -> int:
    
    if len(fragment_list) < 1:
        return 0

    if len(fragment_list) == 1:
        if fragment_list[0] in junction:
            return 1
    
    if not is_possible_subjunction(fragment_list[:2], junction):
        return largest_possible_subjunction(fragment_list[1:], junction)
    
    subj_size = 2
    while subj_size+1 <= len(fragment_list) and is_possible_subjunction(fragment_list[:subj_size+1], junction):
        subj_size += 1
    
    return max(subj_size, largest_possible_subjunction(fragment_list[subj_size:], junction))


def get_effective_fragments_list(input_path: str, fragments_dict: dict, score_threshold=None) -> (list, list):
    """
    get the list of fragments from the fragment identification file,
    then add "unrecognized" fragments in the gap between fragments
    #TODO tothink, should we drop rows of full unrecognized fragments ?
    """
    input = open(input_path, "r")
    lines = input.readlines()
    input.close()
    
    nbr_mol = int(len(lines)/4)

    ac.Fragment.init_junctions(fragments_dict) # init the class var of junctions
    fragment_size = ac.get_original_fragment_size(fragments_dict) # get the size of an original fragment
    unknown_threshold = fragment_size # threshold so start considering a zone as an unrecognized fragment #TODO //2
    
    effective_fragments_list = []
    associated_lines = [] # list of read name / sequence / identified fragments
    n_u = 0

    #output = open(sys.argv[6]+"_eblock_1_join.fasta", 'w')

    transition_dict = {}

    for i in range(nbr_mol):
        fragment_list_string = lines[4*i+2].replace("\n","")
        fragment_list = ac.init_fragment_list(fragment_list_string) # get the list of fragments objects from the string list of the results file
        
        if score_threshold != None:
            # filter fragments with score < threshold
            fragment_list = [f for f in fragment_list if f.score >= score_threshold]
        
        if len(fragment_list) == 2: # empty list = no recognized fragments
            if fragment_list[1].stop_position >= unknown_threshold: # ignore very small reads
                # add 1 unrecognized fragment equal to the whole read
                effective_fragments_list.append([ac.unrecognized_fragment(fragment_list[0].stop_position, fragment_list[1].start_position)])
                associated_lines.append([lines[4*i].replace("\n",""), lines[4*i+1].replace("\n",""), fragment_list_string])
                n_u += 1
            continue


        # create a more realistic fragment list with the unrecognized fragments
        effective_fragments = []

        for j in range(0, len(fragment_list)-1): # parse the list without the "Last" fragment
            fragment = fragment_list[j]
            if fragment.name != "First":
                effective_fragments.append(fragment) # add the fragment in the effective list
            
            distance_with_follower = fragment_list[j+1].start_position - fragment.stop_position
            if distance_with_follower >= unknown_threshold:
                # if the following recognized fragment is more than half a fragment size away
                # there is some unrecognized fragments between the fragment and the following fragment
                nbr_unrecognized_followers = (distance_with_follower+unknown_threshold) // fragment_size
                for k in range(nbr_unrecognized_followers-1):
                    effective_fragments.append(ac.unrecognized_fragment(fragment.stop_position+1+k*fragment_size, fragment.stop_position+(k+1)*fragment_size)) # add the unrecognized fragment in the effective list
                effective_fragments.append(ac.unrecognized_fragment(fragment.stop_position+1+(nbr_unrecognized_followers-1)*fragment_size, fragment_list[j+1].start_position-1)) # add the unrecognized fragment in the effective list

        """if len(fragment_list) >= 4:
            for k in range(1, len(fragment_list)-1):
                if "mol_1_eBlock_1" in fragment_list[k].name and "mol_1_eBlock_1" in fragment_list[k+1].name: #TODO remove

                    output.write(lines[4*i].replace("@",">"))
                    output.write(lines[4*i+1])
                    output.write(lines[4*i+2].replace("('First', 10, [0, 0]), ", ""))

                    transition_name = fragment_list[k].name+"_to_"+fragment_list[k+1].name
                    transition_dict[transition_name] = transition_dict.get(transition_name, 0) +1
                    primers_list = "[ "
                    for primer, p_seq in {"Fw": "GCGCAGCAACTTTGATATCC", "FwC": "CGCGTCGTTGAAACTATAGG", "Rv": "CCTATAGTTTCAACGACGCG", "RvC": "GGATATCAAAGTTGCTGCGC"}.items():
                        if p_seq in lines[4*i+1]:

                            primers_list += "primer_"+primer+str([n for n in range(len(lines[4*i+1])) if lines[4*i+1].startswith(p_seq, n)])+" "
                    output.write(primers_list+"]\n\n")"""

        nbr_frag = len(effective_fragments)

        if nbr_frag > 0:
            effective_fragments_list.append(effective_fragments)
            associated_lines.append([lines[4*i].replace("\n",""), lines[4*i+1].replace("\n",""), fragment_list_string])
        
    #output.close()

    #print("full unrecognized :",str(n_u)+"/"+str(nbr_mol))
    return effective_fragments_list, associated_lines


def save_bases_before(effective_fragments_list, output_path: str) -> None:
    """
    save the number of bases before the fragment
    """
    
    bases_before = {}
    bases_after = {}
    
    max_n_bases = 25
    
    for effective_fragments in effective_fragments_list:
        prev_frag_stop_position = 0
        for fragment in effective_fragments:
            if fragment.name != "Unrecognized":
                nbr_bases_before = fragment.start_position - prev_frag_stop_position
                if nbr_bases_before > max_n_bases: nbr_bases_before = max_n_bases

                if "_Fw" in fragment.name:
                    bases_before[fragment.short_name()] = bases_before.get(fragment.short_name(), {})    
                    increment_dict(nbr_bases_before, bases_before[fragment.short_name()])
                else: # for reverse fragments, the count represents the bases after
                    bases_after[fragment.short_name()] = bases_after.get(fragment.short_name(), {})    
                    increment_dict(nbr_bases_before, bases_after[fragment.short_name()])
                prev_frag_stop_position = fragment.stop_position
    
    # save file
    
    output = open(output_path, "a")
    
    # get fragment names sorted by junction order
    sorted_frag_list = ac.get_junction_short_names()
         

    # save the positions in the molecule
    
    output.write("bases before\nfragment_name;number_occurrences;"+";".join([str(k) for k in range(0, max_n_bases+1)])+"\n")
    
    for fragment_name in sorted_frag_list:
        output.write(fragment_name+";")
        bases_before_fragment = bases_before.get(fragment_name, {})
        output.write(str(sum(bases_before_fragment.values()))+";")
        for i in range(0, max_n_bases+1):
            nbr_bases_before = get_nbr_value_dict(i, bases_before_fragment)
            output.write(str(nbr_bases_before)+";")
        output.write("\n")
        
    output.write("\nbases after\nfragment_name;number_occurrences;"+";".join([str(k) for k in range(0, max_n_bases+1)])+"\n")
    for fragment_name in sorted_frag_list:
        output.write(fragment_name+";")
        bases_after_fragment = bases_after.get(fragment_name, {})
        output.write(str(sum(bases_after_fragment.values()))+";")
        for i in range(0, max_n_bases+1):
            nbr_bases_before = get_nbr_value_dict(i, bases_after_fragment)
            output.write(str(nbr_bases_before)+";")
        output.write("\n")
    output.write("\n")
    output.close()


def save_row_sizes(effective_fragments_list, output_path: str, max_nbr_frag: int) -> None:
    """
    save the number of correct and incorrect rows for each number of fragments
    """
    occurrence_nbr_frag = {} # key = number of fragment in a molecule, value = occurrence in the molecules
    row_size_per_nbr_frag = {} # key = number of fragment in a molecule, value = dict of occurrence per row size
    for effective_fragments in effective_fragments_list:
        nbr_frag = len(effective_fragments)
        
        increment_dict(nbr_frag, occurrence_nbr_frag) # increase the occurrence for this number of fragment in a molecule
        
        frag_names_list = []
        for fragment in effective_fragments:
            frag_names_list.append(fragment.name)
            #frag_names_list.append(''.join([char for char in fragment.name if not char.isdigit()])) # semi-ordered = ignore numbers in fragments

        largest_fw_row = largest_possible_subjunction(frag_names_list, ac.Fragment.junction_fw)
        largest_rv_row = largest_possible_subjunction(frag_names_list, ac.Fragment.junction_rv)
        largest_row = max(largest_fw_row, largest_rv_row)

        # increment the number of subjunctions of this size for this number of fragments in the read
        if largest_row >= 2: #TODO was 2
            row_size_per_nbr_frag[nbr_frag] = row_size_per_nbr_frag.get(nbr_frag, {})
            row_size_per_nbr_frag[nbr_frag][largest_row] = row_size_per_nbr_frag[nbr_frag].get(largest_row, 0) + 1

    # save file    
    
    output = open(output_path, "a")
    
    # save the occurrences of correct and incorrect rows per number of fragments
    output.write("number_of_fragment;"+";".join([str(k) for k in range(1, max_nbr_frag+1)])+";>"+str(max_nbr_frag+1))
    
    max_row_size = len(ac.Fragment.junction_fw)
    output.write("\nnumber_of_correct_rows;")

    for nbr_frag in range(1, max_nbr_frag+1):
        count = get_nbr_value_dict(min(nbr_frag, max_row_size), row_size_per_nbr_frag.get(nbr_frag, {}))
        output.write(str(count)+";")
    output.write("0") # write 0 for the number of correct rows over the maximum number of fragments
    
    #TODO largest = 10 is correct for 13 blocks -> also count in incorrects
    
    output.write("\nnumber_of_incorrect_rows;")
    for nbr_frag in range(1, max_nbr_frag+1):
        count = get_nbr_value_dict(min(nbr_frag, max_row_size), row_size_per_nbr_frag.get(nbr_frag, {}))
        incorrect_rows_nbr = occurrence_nbr_frag.get(nbr_frag,0) - count
        output.write(str(incorrect_rows_nbr)+";")
    sum_after_max = 0
    for nbr_frag in range(max_nbr_frag+1, 1000):
        sum_after_max += occurrence_nbr_frag.get(nbr_frag,0)
    output.write(str(sum_after_max)+"\n\n")
    
    output.close()    
    
    
def save_occurrences_followers(effective_fragments_list: list, output_path: str, bigBlock_num=None) -> None:
    """
    save the number of occurence for each fragment,
    and the number of time it is followed by each fragment
    """
    
    fragments_occurrences = {} # key = fragment name, value = [number of occurrence in the file, sum of score]
    
    fragments_following_fragments_same_dirs = {"First": {}} # key = fragment name, value = dict of occurrences for each following fragment
    fragments_following_fragments_Fw_to_Rvc = {"First": {}}
    fragments_following_fragments_Rvc_to_Fw = {"First": {}}

    for effective_fragments in effective_fragments_list:
        for j, fragment in enumerate(effective_fragments):
            fragment = effective_fragments[j]
            # increase the counter for this fragment in the fragment dict

            occ_list = fragments_occurrences.get(fragment.short_name(), [0,0])
            occ_list[0] += 1 # increment occurrences
            occ_list[1] += fragment.score
            
            fragments_occurrences[fragment.short_name()] = occ_list
                        
            # init followers dict for this fragment if the key doesn't exist
            fragments_following_fragments_same_dirs[fragment.short_name()] = fragments_following_fragments_same_dirs.get(fragment.short_name(), {})


            if fragment.is_fw():
                if j == len(effective_fragments)-1: # last fragment of the read
                    # a last fw fragment is followed by "Last"
                    increment_dict("Last", fragments_following_fragments_same_dirs[fragment.short_name()])

                else: # followed by the next fragment
                    following_fragment = effective_fragments[j+1]
                    if following_fragment.is_fw() or following_fragment.name == "Unrecognized": # fragment of same direction
                        increment_dict(following_fragment.short_name(), fragments_following_fragments_same_dirs[fragment.short_name()])
                    else: # Fw fragment followed by a Rvc fragment
                        fragments_following_fragments_Fw_to_Rvc[fragment.short_name()] = fragments_following_fragments_Fw_to_Rvc.get(fragment.short_name(), {})
                        increment_dict(following_fragment.short_name(), fragments_following_fragments_Fw_to_Rvc[fragment.short_name()])
                    if j == 0: # first fragment
                        # also increment this fragment as a follower of "First"
                        increment_dict(fragment.short_name(), fragments_following_fragments_same_dirs["First"])
                    
            elif fragment.is_rv(): # for reverse fragment, the followers/preceders are exchanged
                if j == 0: # first fragment -> followed by "Last"
                    increment_dict("Last", fragments_following_fragments_same_dirs[fragment.short_name()])
                else: # followed by the fragment before in the list
                    preceding_fragment = effective_fragments[j-1]
                    if preceding_fragment.is_rv() or preceding_fragment.name == "Unrecognized": # fragment of same direction
                        increment_dict(preceding_fragment.short_name(), fragments_following_fragments_same_dirs[fragment.short_name()])
                    #else: do nothing since already counted in the Fw->Rvc
                    if j == len(effective_fragments)-1: # last fragment but its a reverse fragment
                        # also increment this fragment as a follower of "First"
                        increment_dict(fragment.short_name(), fragments_following_fragments_same_dirs["First"])

                if j < len(effective_fragments)-1 and effective_fragments[j+1].is_fw(): # Rvc fragment followed by a Fw fragment
                    fragments_following_fragments_Rvc_to_Fw[fragment.short_name()] = fragments_following_fragments_Rvc_to_Fw.get(fragment.short_name(), {})
                    increment_dict(effective_fragments[j+1].short_name(), fragments_following_fragments_Rvc_to_Fw[fragment.short_name()])

            """

            fragments_following_fragments[fragment.name] = fragments_following_fragments.get(fragment.name, {})

            if j == len(effective_fragments)-1: # last fragment
                # the last fragment is followed by "Last"
                increment_dict("Last", fragments_following_fragments[fragment.name])
            else: # followed by the next fragment
                increment_dict(effective_fragments[j+1].name, fragments_following_fragments[fragment.name])
                if j == 0: # first fragment
                    # also increment this fragment as a follower of "First"
                    increment_dict(fragment.name, fragments_following_fragments["First"])"""


    opposite_dirs_fragments = set(fragments_following_fragments_Fw_to_Rvc) \
                              | set(fragments_following_fragments_Rvc_to_Fw)
    fragments_following_fragments_opposite_dirs = {key: {k: fragments_following_fragments_Fw_to_Rvc.get(key, {}).get(k, 0) +
                                                            fragments_following_fragments_Rvc_to_Fw.get(key, {}).get(k, 0)
                                                         for k in set(fragments_following_fragments_Fw_to_Rvc.get(key, {}))
                                                         | set(fragments_following_fragments_Rvc_to_Fw.get(key, {}))}
                                                   for key in opposite_dirs_fragments}


    save_followers_dict(fragments_following_fragments_same_dirs, fragments_occurrences, output_path, bigBlock_num)
    save_followers_dict(fragments_following_fragments_opposite_dirs, None, output_path, bigBlock_num)
    save_followers_dict(fragments_following_fragments_Fw_to_Rvc, None, output_path, bigBlock_num)
    save_followers_dict(fragments_following_fragments_Rvc_to_Fw, None, output_path, bigBlock_num)


def save_followers_dict(fragments_following_fragments, fragments_occurrences, output_path, bigBlock_num):

    output = open(output_path, "a")

    # get fragment names sorted by junction order
    sorted_frag_list = ac.get_junction_short_names(bigBlock_num)

    #TODO used for Fw Rvc followers
    #sorted_frag_list = [frag + dir for dir in ['_Fw', '_Rvc'] for frag in sorted_frag_list if "mol_"+bigBlock_num+"_" in frag]

    if fragments_occurrences is not None:
        # save number of occurrences, times being first and number of time followed by each fragments
        output.write("fragment_name;number_occurrences;times_first;"+";".join(sorted_frag_list)+";End;Unrecognized\n")
    else:
        output.write("fragment_name;"+";".join(sorted_frag_list)+"\n")

    #print(sorted_frag_list)
    for fragment_name in sorted_frag_list:

        output.write(fragment_name+";")
        if fragments_occurrences is not None:
            #save the number of occurrences for this fragment in the file
            nbr_occurrences = fragments_occurrences.get(fragment_name, [0])[0]
            output.write(str(nbr_occurrences)+";")
            #save the number of times this fragment is at the beginning of a molecule in the file
            first_times = get_nbr_value_dict(fragment_name, fragments_following_fragments["First"])
            output.write(str(first_times)+";")
        #save the occurrences of following fragments for this fragment
        for follower in sorted_frag_list:
            following_times = get_nbr_value_dict(follower, fragments_following_fragments.get(fragment_name, None))
            output.write(str(following_times)+";")

        if fragments_occurrences is not None:
            #save the number of times this fragment is at the end of a molecule in the file
            last_times = get_nbr_value_dict("Last", fragments_following_fragments.get(fragment_name, None))
            output.write(str(last_times)+";")

            #save the number of times this fragment is followed by an unrecognized fragment
            unrecognized_times = get_nbr_value_dict("Unrecognized", fragments_following_fragments.get(fragment_name, None))
            output.write(str(unrecognized_times)+";")

        output.write("\n")

    output.write("\n")
    output.close()
    #print("unrecognized :",fragments_occurrences.get("Unrecognized", [0])[0])


def save_correct_junctions(effective_fragments_list: list, output_path: str) -> None:
    """
    save the number of correct 2-junctions for assemblies of each size,
    and the number of incorrect 2-junctions implying a start or stop fragment
    """
        
    max_n_frag = 21
    
    # init assembly length dict -> mail 04/05/22
    assembly_length = {}
    for i in range(2, max_n_frag+1):
        assembly_length[i] = [{}, {}] # [correct_row, incorrect_row]
        fragments_names_list = ac.get_junction_short_names()
        prev_frag_name = fragments_names_list[0]
        
        for next_frag_name in fragments_names_list[1:]: # correct rows : N to N+1
            junction_name = prev_frag_name+"_"+next_frag_name
            assembly_length[i][0][junction_name] = 0
            prev_frag_name = next_frag_name
        
        for frag_name in fragments_names_list: # incorrect rows : start to any
            junction_name = fragments_names_list[0]+"_"+frag_name
            assembly_length[i][1][junction_name] = 0
        for frag_name in fragments_names_list: # incorrect rows : stop to any
            junction_name = fragments_names_list[-1]+"_"+frag_name
            assembly_length[i][1][junction_name] = 0
           
    for effective_fragments in effective_fragments_list:
        nbr_frag = len(effective_fragments)
        if nbr_frag >= max_n_frag:
            nbr_frag = max_n_frag # for > max_n_frag fragments
        
        frag_names_list = []
        
        for fragment in effective_fragments:
            frag_names_list.append(fragment.name)
        
        largest_fw_row = largest_possible_subjunction(frag_names_list, ac.Fragment.junction_fw)
        largest_rv_row = largest_possible_subjunction(frag_names_list, ac.Fragment.junction_rv)
        largest_row = max(largest_fw_row, largest_rv_row)
        
        if nbr_frag > 1:
            if largest_fw_row < largest_rv_row:
                effective_fragments.reverse()
            prev_frag_name = effective_fragments[0].short_name()
            for next_frag in effective_fragments[1:]:
                junction_name = prev_frag_name+"_"+next_frag.short_name()
                if nbr_frag == largest_row: # correct junction    
                    if junction_name in assembly_length[nbr_frag][0]:
                        assembly_length[nbr_frag][0][junction_name] += 1     
                else: # incorrect junction                    
                    if junction_name in assembly_length[nbr_frag][1]:
                        assembly_length[nbr_frag][1][junction_name] += 1   
                prev_frag_name = next_frag.short_name()
    
    # save in file
    
    output = open(output_path, "a")
    output.write("correct_rows;")

    for key in assembly_length[2][0].keys():
        output.write(key+";")
    output.write(";incorrect_rows;")
    for key in assembly_length[2][1].keys():
        output.write(key+";")
    output.write("\n")   
    for i in range(2,max_n_frag+1):
        output.write(str(i)+";")
        for value in assembly_length[i][0].values():
            output.write(str(value)+";")
        output.write(";"+str(i)+";")
        for value in assembly_length[i][1].values():
            output.write(str(value)+";")
        output.write("\n")
    output.write("\n")
    output.close()


def save_specific_junctions(effective_fragments_list: list, associated_lines: list, output_path: str) -> None:
    """
    save reads parts containing defined junctions
    """

    
    junctions_list = [["mol_5_eBlock_1", "mol_5_eBlock_1"]]#, ["Oligo_to_Block02", "Oligo_to_Block04"], ["Oligo_to_Block03", "Oligo_to_Block05"], ["Oligo_to_Block04", "Oligo_to_Block06"]]

    saved_reads_dict = {}
    
    for junction in junctions_list:
        
        junction_filename = "_".join(junction)+".txt"
        output = open(output_path.replace(".txt", "_"+junction_filename), "w")

        fw_junction = ["a1_Fw", "a1_Rv"]
        #fw_junction = [name+"_Fw" for name in junction] #
        rv_junction = [name+"_Rv" for name in junction]

        for i, effective_fragments in enumerate(effective_fragments_list):
            effective_fragments = [ k for k in effective_fragments if k.short_name() != "Unrecognized"]
            #print(effective_fragments)

            if "".join(fw_junction) in "".join(fragment.name for fragment in effective_fragments):
                #output.write("\n".join(associated_lines[i])) # read name, read sequence, read fragments list
                #output.write("\n\n")
                begin_junc_fragment = [frag for frag in effective_fragments if frag.name == fw_junction[0]][0]
                end_junc_fragment = [frag for frag in effective_fragments if frag.name == fw_junction[-1]][0]

                start_first, stop_first = begin_junc_fragment.start_position, begin_junc_fragment.stop_position
                start_last, stop_last = end_junc_fragment.start_position, end_junc_fragment.stop_position

                output.write("@"+associated_lines[i][0]+"\n") # read name, read sequence, read fragments list
                output.write(associated_lines[i][2]+"\n")

                output.write(fw_junction[0]+": "+associated_lines[i][1][start_first:stop_first]+"\n") # junction of 2 blocks, writes the first one
                output.write("between: "+associated_lines[i][1][stop_first:start_last]+"\n") # junction of 2 blocks, writes what is between the blocks
                output.write(fw_junction[-1]+": "+associated_lines[i][1][start_last:stop_last]+"\n") # junction of 2 blocks, writes the last one

                output.write("\n\n")
            elif "".join(rv_junction) in "".join(fragment.name for fragment in effective_fragments[::-1]):
                #output.write("\n".join(associated_lines[i])) # read name, read sequence, read fragments list
                #output.write("\n\n")
                continue#TODO
                begin_junc_fragment = [frag for frag in effective_fragments if frag.name == fw_junction[0]][0]
                end_junc_fragment = [frag for frag in effective_fragments if frag.name == fw_junction[-1]][0]

                start_first, stop_first = begin_junc_fragment.start_position, begin_junc_fragment.stop_position
                start_last, stop_last = end_junc_fragment.start_position, end_junc_fragment.stop_position

                output.write("@"+associated_lines[i][0]+"\n") # read name, read sequence, read fragments list
                output.write(associated_lines[i][2]+"\n")
                output.write(ac.dfr.reverse_complement(associated_lines[i][1][stop_first:start_last])+"\n")
                output.write("\n\n")
        output.close()


def filter_fragments_file(effective_fragments_list: list, associated_lines: list, output_path: str, fragment_number) -> None:
    """
    removes reads with a given number of fragments
    """
    
    output = open(output_path, "w")
    for j in range(len(effective_fragments_list)):
        if len(effective_fragments_list[j]) == fragment_number:
            output.write(associated_lines[j][0]+"\n") 
            output.write(associated_lines[j][1]+"\n")

            simplified_frag_list = []
            for frag in effective_fragments_list[j]:
                if frag.name == "Unrecognized":
                    simplified_name = frag.name
                else: # short name for the fragments
                    simplified_name = frag.name.split("_")[-2]+"_"+frag.name.split("_")[-1]
                
                simplified_frag_list.append(simplified_name)
                
            #output.write(" - ".join(simplified_frag_list)+"\n")
            output.write(associated_lines[j][2]+"\n")
            
            """
            additional_unrecognized_info = [] # print additional line for fragments around unrecognized region
            for i, s_frag in enumerate(simplified_frag_list):
                if s_frag == "(Unrecognized)":
                    if i == 0:
                        frag_before_U = "(Start)"
                    elif simplified_frag_list[i-1] == "(Unrecognized)":
                        continue
                    else:
                        frag_before_U = simplified_frag_list[i-1]
                    for next_frag in (simplified_frag_list+["(Stop)"])[i+1:]:
                        if next_frag != "(Unrecognized)":
                            frag_after_U = next_frag
                            break
                    additional_unrecognized_info.append(frag_before_U+frag_after_U)

            if len(additional_unrecognized_info) > 0:
                output.write("- Unrecognized region: "+" & ".join(additional_unrecognized_info)+"\n")
                
            additional_switch_info = [] # print additional line for fragments that switches Fw and Rv
            for i, s_frag in enumerate(simplified_frag_list[:-1]):
                if ("Fw" in s_frag and "Rv" in simplified_frag_list[i+1]) or ("Rv" in s_frag and "Fw" in simplified_frag_list[i+1]):
                    additional_switch_info.append(s_frag+simplified_frag_list[i+1])
            for frag_pair in additional_unrecognized_info:
                if "Fw" in frag_pair and "Rv" in frag_pair:
                    additional_switch_info.append(frag_pair)
                    
            if len(additional_switch_info) > 0:
                output.write("- Orientation switch: "+" & ".join(additional_switch_info)+"\n")
            """
            output.write("\n")
    
    output.close()


def sort_fragments(effective_fragments_list: list, associated_lines: list, outputdir_path: str):
    """
    extract and sort all fragments sequences into different files
    """
    
    fragment_sequences_dict = {}
    # TODO quick fix to get the reads scores
    #reads_dict = dfr.read_fastq("0_data/data_fev_24/FLG01.fastq",use_score=False)

    for i, effective_fragments in enumerate(effective_fragments_list):
        if len(effective_fragments) >= 1: # can be for mono frag reads, or for all fragments in the read
            for fragment in effective_fragments:
                frag_seq = associated_lines[i][1]#[fragment.start_position:fragment.stop_position]
                if len(frag_seq) > 580: #TODO filter
                    continue

                frag_name = associated_lines[i][0][1:]
                # search the score in the original fastq
                #frag_score = reads_dict[frag_name][2][fragment.start_position:fragment.stop_position]
                if "RvC" in fragment.name:
                    frag_seq = ac.dfr.reverse_complement(frag_seq)
                base_frag_name = fragment.name.replace("_Fw", "").replace("_RvC", "")
                fragment_sequences_dict[base_frag_name] = fragment_sequences_dict.get(base_frag_name, [])
                fragment_sequences_dict[base_frag_name].append(frag_seq)#, frag_score])

    for base_frag_name, seq_list in sorted(fragment_sequences_dict.items()):
        output = open(outputdir_path+"/"+base_frag_name+".fastq", "w")
        for i, seq in enumerate(seq_list):
            output.write("@"+base_frag_name+"_"+str(i)+"\n")
            output.write(seq+"\n")
            output.write("+\n")
            output.write("\n")

        output.close()
        print(base_frag_name,":", len(seq_list))
    

def extract_from_fragment(effective_fragments_list: list, associated_lines: list, outputdir_path: str):
    """
    get the sequences starting from a fragment
    """
    frag_to_extract = "molecule_9_eBlock_08_Fw"
    
    output = open(output_path, "w")
    for i, effective_fragments in enumerate(effective_fragments_list):
        for fragment in effective_fragments:
            
            if frag_to_extract in fragment.name:
                frag_seq = associated_lines[i][1][fragment.start_position:]
                #output.write("@"+associated_lines[i][0]+"\n") 
                output.write(frag_seq+"\n")
                #output.write("\n")
                #output.write("\n")
    output.close()
    
    
def save_single_fragments_reads(effective_fragments_list: list, associated_lines: list, output_path: str, selected_fragment=""):
    """
    save reads with only one fragment and (optional) with a defined name
    """

    # part to ount the restrictions sites in the single fragments reads
    sites_to_count=["GGTCTC", "GAGACC"]
    site_counter_dict = {site:0 for site in sites_to_count}
    single_fragments_count = 0

    output = open("concat_NT_both_bc", "a")
    for i, effective_fragments in enumerate(effective_fragments_list):
        fragment_name = effective_fragments[0].name

        # filter fragments that have only one block, or are only Unrecognized
        if len(effective_fragments) == 1 and "_" in str(effective_fragments) and selected_fragment in fragment_name: #TODO
        #if not "Block" in associated_lines[i][2]:
            #single_fragments_count += 1
            frag_seq = associated_lines[i][1]

            # print the rc of the sequence if it's a reversed read
            if "Rv" in effective_fragments[0].name:
                frag_seq = ac.dfr.reverse_complement(frag_seq)
                fragment_name = fragment_name.replace("Rv", "Fw")

            """output.write("@"+str(i)+"_"+fragment_name+"_"+str(len(frag_seq))+"\n")
            output.write(frag_seq+"\n")
            output.write("+\n")
            output.write("+\n")""""""
            output.write("@"+associated_lines[i][0]+"\n")
            for l in associated_lines[i][1:]:
                output.write(str(l)+"\n")
            output.write("\n")"""

            for site in sites_to_count:
                if site in frag_seq:
                    site_counter_dict[site] += 1

    output.close()
    forward_site_pc = round(100*site_counter_dict["GGTCTC"]/len(effective_fragments_list),2)
    reverse_site_pc = round(100*site_counter_dict["GAGACC"]/len(effective_fragments_list),2)
    print(len(effective_fragments_list),"reads in file => GGTCTC :",forward_site_pc,"% ; GAGACC :",reverse_site_pc,"%\n")



    

def extract_unrecognized_zones(input_path: str, output_path: str):
    """
    save only zones not unrecognized as fragments
    """
    
    input = open(input_path, "r")
    lines = input.readlines()
    input.close()
    
    nbr_mol = int(len(lines)/4)
    unknown_threshold = 50 # threshold so start considering a zone as an unrecognized fragment
    
    output = open(output_path, "w")
    
    for i in range(nbr_mol):
        
        sequence = lines[4*i+1].replace("\n","")
        fragment_list_string = lines[4*i+2].replace("\n","")
        fragment_list = ac.init_fragment_list(fragment_list_string) # get the list of fragments objects from the string list of the results file
        
        # when the first fragment is more than the threshold away from the start of the read
        if fragment_list[1].start_position >= unknown_threshold:
            u_zone = sequence[0:fragment_list[1].start_position] # there is an unrecognized fragment before
            output.write("@U\n"+u_zone+"\n+\n+\n")
        
        for j in range(1, len(fragment_list)-1): # parse the list without the "First" and "Last" fragments
            fragment = fragment_list[j]
            
            distance_with_follower = fragment_list[j+1].start_position - fragment.stop_position
            
            if distance_with_follower >= unknown_threshold:
                # there is some unrecognized fragments between the fragment and the following fragment
                u_zone = sequence[fragment.stop_position:fragment_list[j+1].start_position]
                output.write("@U\n"+u_zone+"\n+\n+\n")

    output.close()
    
    
def get_list_assemblies(effective_fragments_list: list, associated_lines: list, output_path: str):
    """
    get the list of all assemblies in the reads
    """
    
    assemblies_dict = {}
    frag_dict = {}
    lines_dict = {}
    RC_TAB = str.maketrans("ACTG", "TGAC")

    for i, effective_fragments in enumerate(effective_fragments_list):
        frag_seq = associated_lines[i][1]
        assembly_list = []
        if str(effective_fragments).count("Rv") > str(effective_fragments).count("Fw"): # revert list of frags starting with a rc fragment
            effective_fragments = effective_fragments[::-1]
            frag_seq = frag_seq.translate(RC_TAB)[::-1]
        for fragment in effective_fragments:
            if fragment.name != "Unrecognized":
                assembly_list.append(fragment.short_name())
                frag_dict[fragment.short_name()] = frag_dict.get(fragment.short_name(), 0) + 1
            else:
                assembly_list.append("U")
        assembly_name =  "+".join(assembly_list)

        assemblies_dict[assembly_name] = assemblies_dict.get(assembly_name, 0) +1
        lines_dict[assembly_name] = lines_dict.get(assembly_name, [])
        lines_dict[assembly_name].append(frag_seq)

    output = open(output_path, "w")
    for (assembly, nbr_occ) in sorted(assemblies_dict.items(), key=lambda kv: kv[1], reverse=True):
        output.write(assembly+" "+str(nbr_occ)+"\n")
    output.close()
    """
    for assembly, nbr_occ in assemblies_dict.items():
        if "U" in assembly and nbr_occ > 300:
            output = open(output_path+"_"+assembly+".txt", "w")
            for frag_seq in lines_dict[assembly]:
                output.write(frag_seq+"\n")
            output.close()"""


def count_transitions(effective_fragments_list: list):
    
    transitions_dict = {}
    for i in range(1,5):

        transitions_dict["a"+str(i)] = 0
        transitions_dict["a"+str(i)+"_a"+str(i)] = 0
        for j in range(i+1, 5):
            transitions_dict["a"+str(i)+"_a"+str(j)] = 0
            
    for i, effective_fragments in enumerate(effective_fragments_list):
        
        names_list = [fragment.short_name() for fragment in effective_fragments if fragment.short_name() != "Unrecognized"]

        if len(names_list) == 1:
            transitions_dict[names_list[0]] += 1
            continue        
        
        for k in range(len(names_list)-1):
            transition_1, transition_2 = names_list[k], names_list[k+1]
            if transition_1+ "_" + transition_2 in transitions_dict.keys():
                transition_name = transition_1+ "_" + transition_2
            else:
                transition_name = transition_2+ "_" + transition_1
            transitions_dict[transition_name] += 1

            
    print(transitions_dict)


def blast_file_sorting():
    """
    use an output file from the blast referrence assignation
    sort the reads id by referrence
    """
    input_blast_txt = "0_data/images_18k/blast_on_all.txt"
    output_result = "0_data/images_18k/all_reads_id_by_referrences.txt"

    ref_dict = {}

    # read the blast output to get the list of read id for each referrence
    with open(input_blast_txt, 'r') as input:
        line = input.readline()
        while line != "":
            line_data = line.split("\t")
            read_id = line_data[0]
            ref_name = line_data[1]
            # the read corresponds to the forward ref if the start index is lower than end index
            # else it is a reverse complement read
            is_forward = int(line_data[2]) <= int(line_data[3])
            #if is_forward:
            #    ref_name += "_FW"
            #else:
            #    ref_name += "_RC"
            ref_dict[ref_name] = ref_dict.get(ref_name, [])
            ref_dict[ref_name].append(read_id)
            line = input.readline()

    print({k: len(v) for k, v in sorted(ref_dict.items(), key=lambda item: len(item[1]))})
    return #TODO
    print("blast file loaded !")
    # save sorted ref in file
    with open(output_result, 'w') as output:
        for ref_name, id_list in sorted(ref_dict.items()):
            output.write(ref_name+" :")
            for read_id in id_list:
                output.write(" "+read_id)
            output.write("\n")
    print("results saved !")


def generate_reference_reads(effective_fragments_list: list, associated_lines: list, fragments_dict: dict, reference_output_path: str):
    """
    generate a fasta with the reference sequence for each read id,
    used for basecalling training
    need only one reference per read, no assembly of molecules
    """

    references_by_read_dict = {} # store the reference sequence by read id

    for i, effective_fragments in enumerate(effective_fragments_list):
        read_id = associated_lines[i][0][1:]
        if len(effective_fragments) == 1:
            fragment_name = effective_fragments[0].name
            if fragment_name != "Unrecognized":
                references_by_read_dict[read_id] = fragments_dict[fragment_name]

    dfr.save_dict_to_fasta(references_by_read_dict, reference_output_path)



def length_analysis(input_path: str, output_path=None):
    """
    analysis on read length distributions
    """
    input = open(input_path, "r")
    #input = open("concat_NT_both_bc", "r")
    lines = input.readlines()
    input.close()
    
    nbr_mol = int(len(lines)/4)
    
    distribution_fw = {}
    distribution_rv = {}
    #frag_number_distribution = {} # number of reads with [key] fragments
    #length_by_frag_number = {}
    length_interval = 5 # size of the interval of length columns

    fw_count = 0
    rv_count = 0
    
    for i in range(nbr_mol):
        
        sequence = lines[4*i+1].replace("\n","")
        fragment_list_string = lines[4*i+2].replace("\n","")
        fragment_list = ac.init_fragment_list(fragment_list_string) # get the list of fragments objects from the string list of the results file
    
        seq_length = length_interval*(len(sequence)//length_interval)

        fragment = fragment_list[1].name
        if "Fw" in fragment:
            distribution_fw[seq_length] = distribution_fw.get(seq_length, 0) + 1 # +1 (number of read) +seq_length (DNA content)
            fw_count += 1
        elif "Rv" in fragment:
            distribution_rv[seq_length] = distribution_rv.get(seq_length, 0) + 1 # +1 (number of read) +seq_length (DNA content)
            rv_count += 1
        else:
            continue


    #print(length_distribution_dict)
    #for fragment, distribution in length_distribution_dict.items():
        #if fragment == "Last":
        #    fragment = "Unrecognized"
    print("fw:"+str(fw_count)+";rv:"+str(rv_count))

    #plt.subplot(211)
    #print(sorted(distribution.items()))
    ax = plt.gca()
    ax.set_xlim([0, 1200])#max(distribution_fw.keys())+10])
    ax.set_ylim([0, 10])
    plt.bar(list(distribution_fw.keys()), [100*k/fw_count for k in distribution_fw.values()], length_interval, color='g')

    plt.ylabel("% of reads")# (Fw)")
    plt.title(input_path.split("/")[-1].replace(".txt", "").replace("extraction_",""))

    """
    plt.subplot(212)
    ax = plt.gca()
    ax.set_xlim([0, 300])#max(distribution.keys())+10])
    ax.set_ylim([0, 100])
    plt.bar(list(distribution_rv.keys()), [100*k/rv_count for k in distribution_rv.values()], length_interval, color='r')

    plt.xlabel("Length of sequence")
    plt.ylabel("% of reads (RvC)")"""

    graph_name = input_path.split("/")[-1].replace(".txt", ".png")
    if output_path:
        plt.savefig(output_path)
    else:
        plt.show()


# =================== main ======================= #
if __name__ == '__main__':
    
    parser = argparse.ArgumentParser(description='get_effective_fragments_lists of fragments')
    parser.add_argument('-i', action='store', dest='input_path', required=True,
                        help='file to read the extracted fragments')
    parser.add_argument('-f', action='store', dest='fragments_path', required=True,
                        help='file to read the originals fragments')
    parser.add_argument('-o', action='store', dest='output_path', required=True,
                        help='file to save the result')
    parser.add_argument('-n', action='store', dest='block_num', required=False,
                        help='file to save the result')

    arg = parser.parse_args()
    
    output_path = arg.output_path
    
    if os.path.isfile(output_path): os.remove(output_path)

    fragments_dict = dfr.read_fasta(arg.fragments_path)
    
    effective_fragments_list, associated_lines = get_effective_fragments_list(arg.input_path, fragments_dict)

    #save_bases_before(effective_fragments_list, output_path)
    #save_row_sizes(effective_fragments_list, arg.output_path, 50)
    save_occurrences_followers(effective_fragments_list, output_path, arg.block_num)
    #save_correct_junctions(effective_fragments_list, output_path)
    #save_specific_junctions(effective_fragments_list, associated_lines, output_path)
    #filter_fragments_file(effective_fragments_list, associated_lines, output_path+"_filtered.fastq", 11)
    #sort_fragments(effective_fragments_list, associated_lines, "2_analysis/data_oct_24_2/distribution_fragments")
    #extract_from_fragment(effective_fragments_list, associated_lines, output_path)
    #save_single_fragments_reads(effective_fragments_list, associated_lines, output_path)
    #extract_unrecognized_zones(arg.input_path, output_path+"_fullU_zones.fastq")
    #get_list_assemblies(effective_fragments_list, associated_lines, output_path)
    #count_transitions(effective_fragments_list, associated_lines)
    #generate_reference_reads(effective_fragments_list, associated_lines, fragments_dict, output_path)
    #length_analysis(arg.input_path, output_path)

    #TODO show distribution of score for each fragments
