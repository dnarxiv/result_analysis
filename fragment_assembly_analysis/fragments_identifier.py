#!/usr/bin/env python3

import sys
import os
import inspect
import argparse
import subprocess
import time


currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0, os.path.dirname(parentdir)+"/synthesis_modules")
import dna_file_reader as dfr


# scp -r oboulle@genossh.genouest.org:/groups/dnarxiv/jleblanc_data_analysis/demultiplexed_data/20220118_jleblanc_demultiplexed/barcode* .
"""
this script aims to map multiple references fragments in a read produced by an assembly of fragments
if the read is supposed to contains only 1 fragment,
it is suggested to use blast (il will be millions time faster)

# create blast db
makeblastdb -dbtype nucl -in references.fasta -parse_seqids -out references_db

# run blastn, get best aligned ref from db for each reads of query, use 80 cpu, use megablast, result in column : /read_id/ref_id/start_in_ref/end_in_ref/start_in_read/end_in_read/
blastn -query reads.fasta -db references_db -out blast_on_reads.txt -outfmt "6 qseqid sseqid sstart send qstart qend" 
    -max_hsps 1 -max_target_seqs 1 -task megablast -num_threads 80 &
"""



class Aligned_fragment:
    
    def __init__(self, name, score, start_index, stop_index):
        
        self.name = name
        self.score = score
        self.start_index = start_index
        self.stop_index = stop_index
    
    
    def __repr__(self):
        """
        what is returned when a print(aligned_fragment) is done
        """
        return str((self.name, round(self.score, 1), [self.start_index, self.stop_index]))



def rec_extract_list_frag(sequence: str, fragments_dict: dict, score_threshold: float, aligned_frags: dict = None, offset_index: int = 0) -> list:
    """
    recursive function to find all the fragments contained in the sequence
    find the fragment with the best alignment score with the sequence
    re apply this function to the 2 sub sequences before and after the alignment
    sequence = [sub_A][alignment_with_frag_X][sub_B] -> return list composed of (extract on sub_A) + frag_X + (extract on sub_B)
    end of recursion when the best alignment score is too low or sub sequence is too short (sub sequences gets shorter with each recursion level)
    :return: list of Aligned_fragments (fragment_name; [start_index;stop_index]; alignment_score) of the fragments contained in the sequence
    """
    
    list_frags = [] # an empty list will be returned if no alignment with a sufficient score is found
    
    if aligned_frags is None: # the recursion carries some of aligned fragments to avoid redundant computing of the alignment
        aligned_frags = {}
    
    for fragment_name, fragment in fragments_dict.items(): # loop over possible fragments that could be found in the sequence

        if fragment_name not in aligned_frags:
            # compute the local alignment of this fragment in the input sequence
            align_score, start_index, stop_index = get_alignment_score(sequence, fragment)
            # save the aligned indexes related to the initial full sequence by translating with the offset index
            aligned_frags[fragment_name] = Aligned_fragment(fragment_name, align_score, offset_index+start_index, offset_index+stop_index)

            
    # get the fragment with the highest alignment score in the dict
    best_aligned_frag = aligned_frags[max(aligned_frags, key=lambda k: aligned_frags[k].score)]

    # fixed threshold to consider the fragment is in the sequence; 0=no alignment, 10=perfect alignment
    if best_aligned_frag.score >= score_threshold:

        list_frags = [best_aligned_frag] # init the resulting fragments list with the best aligned fragment
        
        # recursive call of the function on the subsequence before the alignment
        sub_seq_before_frag = sequence[:best_aligned_frag.start_index-offset_index] # extract the sub sequence present before the aligned fragment
        
        if len(sub_seq_before_frag) > 10:
            # create a dict of frags already aligned that can be reused for the next alignments
            # keep the frags if the alignment indexes fit in the sub sequence before frag
            # also keep the frags with low score, because they can't exist elsewhere in the subsequence and so don't need to be recomputed
            kept_aligned_frags = {f: v for f,v in aligned_frags.items() if v.stop_index <= best_aligned_frag.start_index or v.score < score_threshold}

            # the offset index of the sub sequence will be the same offset index
            list_frags_before_frag = rec_extract_list_frag(sub_seq_before_frag, fragments_dict, score_threshold, aligned_frags=kept_aligned_frags, offset_index=offset_index)
            list_frags = list_frags_before_frag + list_frags # add the fragments at the beginning of the list
        
        # recursive call of the function on the subsequence after the alignment
        sub_seq_after_frag = sequence[best_aligned_frag.stop_index-offset_index:] # extract the sub sequence present after the aligned fragment
        
        if len(sub_seq_after_frag) > 10:  
            # create a dict of frags already aligned that can be reused for the next alignments
            # keep the frags if the alignment indexes fit in the sub sequence after frag
            # also keep the frags with low score, because they can't exist elsewhere in the subsequence and so don't need to be recalculated
            kept_aligned_frags = { f: v for f,v in aligned_frags.items() if v.start_index >= best_aligned_frag.stop_index or v.score < score_threshold}

            # the offset index of the sub sequence will be equal to the index of the end of the alignment
            list_frags_after_frag = rec_extract_list_frag(sub_seq_after_frag, fragments_dict, score_threshold, aligned_frags=kept_aligned_frags, offset_index=best_aligned_frag.stop_index)
            list_frags = list_frags + list_frags_after_frag # add the fragments at the end of the list
            
    return list_frags


def get_alignment_score(sequence, fragment) -> (float, int, int):
    """
    call the c++ script for SmithWaterman alignment
    get the result of the local alignment of the fragment on the sequence
    returns the alignment score, the index of the start of the alignment on the sequence, the index of the end of alignment
    """
    sw_script = currentdir+"/SmithWatermanCPP/SmithWaterman"
    sw_command = sw_script+' '+sequence+' '+fragment
    terminal_result = subprocess.Popen('/bin/bash -c "$SWALIGN"', shell=True, env={'SWALIGN': sw_command}, stdout=subprocess.PIPE).stdout.read()
    score, start_align, stop_align = terminal_result.decode('utf-8').split(" ")
    
    return float(score), int(start_align), int(stop_align)


def get_p5_p3(list_frag: list, sequence: str, fragments_dict: dict) -> list:
    """
    adds ["5p", "3p"] to each fragment in the list if it starts and ends with the 4 bases of the theorical fragment 
    """
    overhang_size = 4 # defined size of overhang
    
    p5_p3_list_frag = []
    for frag_name, position, score in list_frag:
        theorical_fragment = fragments_dict.get(frag_name, "____")
        read_fragment = sequence[position[0]:position[1]]
        
        if read_fragment[:4] == theorical_fragment[:4]:
            p5_presence = "5p"
        else:
            p5_presence = "_"
            
        if read_fragment[-4:] == theorical_fragment[-4:]:
            p3_presence = "3p"
        else:
            p3_presence = "_"
        p5_p3_list_frag.append((frag_name, position, p5_presence, p3_presence, score))
                               
    return p5_p3_list_frag


def low_priority_fragments_extraction(list_frag, sequence, other_fragments_dict):
    """
    2nd extraction that comes after the first one with other smaller fragments
    """
    #TODO test with the class update
    score_threshold = 7
    completed_list_frag = []
    index = 0
    for aligned_fragment in list_frag:
        if index == 0: # try the extraction on the subsequence before the first aligned fragment
            extracted_list_frag = rec_extract_list_frag(sequence[:aligned_fragment.start_index], other_fragments_dict, score_threshold)
            completed_list_frag = extracted_list_frag + completed_list_frag
        
        completed_list_frag += [aligned_fragment] # add the aligned fragment
        
        if index < len(list_frag)-1: # try the extraction on the subsequence between 2 aligned fragments
            start_next_frag = list_frag[index+1].start_index
            if start_next_frag - aligned_fragment.stop_index > 0:
                extracted_list_frag = rec_extract_list_frag(sequence[aligned_fragment.stop_index:start_next_frag], other_fragments_dict, score_threshold, offset_index=aligned_fragment.stop_index)
                completed_list_frag = completed_list_frag + extracted_list_frag
        elif len(sequence) - aligned_fragment.stop_index > 0: # try the extraction on the subsequence after the last aligned fragment
            extracted_list_frag = rec_extract_list_frag(sequence[aligned_fragment.stop_index:], other_fragments_dict, score_threshold, offset_index=aligned_fragment.stop_index)
            completed_list_frag = completed_list_frag + extracted_list_frag
           
        index += 1

    return completed_list_frag
    
    
def molecules_analysis(fastq_file_path: str, fragments_fasta_path: str, output_path: str, score_threshold: float, other_fragments_fasta_path: str = None) -> None:
    """
    extract the list of fragments for each molecule of the fastq file
    use a fasta file of reference fragments that should be found in the reads
    """
    molecule_dict = dfr.read_fastq(fastq_file_path)
    fragments_dict = dfr.read_fasta(fragments_fasta_path)
    
    if other_fragments_fasta_path is not None:
        other_fragments_dict = dfr.read_fasta(other_fragments_fasta_path)

    if molecule_dict == {}:
        print("error fragment identification, empty input :",fastq_file_path)
        return
    
    if fragments_dict == {}:
        print("error fragment identification, empty fragments dict :",fragments_fasta_path)
        return
    
    start_time = time.time()
        
    output = open(output_path, "w")
    mol_count = 0
    total_reads_count = len(molecule_dict)

    for read_name, sequence in molecule_dict.items():
        if sequence == "":
            continue
        
        read_time = time.time()
        extracted_list_frag = rec_extract_list_frag(sequence, fragments_dict, score_threshold)
        if other_fragments_fasta_path is not None:
            extracted_list_frag = low_priority_fragments_extraction(extracted_list_frag, sequence, other_fragments_dict)
        
        #p5_p3_list_frag = get_p5_p3(extracted_list_frag, sequence, fragments_dict)
        
        # add 2 fictional empty fragments at the beginning and end
        # it allows to know if a fragment is the first or the last in a molecule
        framed_list_frag = [("First", 10, [0, 0])] + extracted_list_frag + [("Last", 10, [len(sequence), len(sequence)])] 
        
        output.write("@"+read_name+"\n")
        output.write(sequence+"\n")
        output.write(str(framed_list_frag)+"\n")
        output.write("\n")
        
        mol_count += 1
        print(str(mol_count)+"/"+str(total_reads_count), "", str(round(time.time() - read_time, 2)), " s" + " "*100, end='\r')
        
        #if mol_count >= 2:#TODO reads subpart of reads to tests
         #   break
        
    output.close()
    print(str(round(time.time() - start_time, 2)), " seconds" + " "*100)


def save_fw_rvc_fragments() -> None:
    """
    take a dir of fragments files and add the lines of reverse complements of fragments
    """
    input_path = "0_data/fragments"
    output_path = "0_data/fragments"


    for filename in os.listdir(input_path):
        if ".fasta" in filename and not "_fw_rvc" in filename:
            fasta_input_path = os.path.join(input_path, filename)
            fasta_output_path = os.path.join(output_path, filename.replace(".fasta","_fw_rvc.fasta"))
    
            fragments_dict = dfr.read_fasta(fasta_input_path)
            output_fragment_dict = {}
            
            for fragment_name, sequence in fragments_dict.items():
                fragment_name = fragment_name.replace(" ", "_")
                output_fragment_dict[fragment_name+"_Fw"] = sequence
                output_fragment_dict[fragment_name+"_RvC"] = dfr.reverse_complement(sequence)
        
            dfr.save_dict_to_fasta(output_fragment_dict, fasta_output_path)


def split():
    input = "data/fragments/mol4_payload_fw_rvc.fasta"
    output = "data/fragments/mol4_payload_fw_rvc_splited.fasta"
    fragments_dict = dfr.read_fasta(input)
    output_dict = {}
    split_number = 5 # number of divisions
    
    for key, value in fragments_dict.items():
        split_size = len(value)//split_number
        for i in range(split_number):
            if "Fw" in key:
                output_dict[key+"_split_"+str(i)] = value[i*split_size:(i+1)*split_size]
            else:
                output_dict[key+"_split_"+str(split_number-1-i)] = value[i*split_size:(i+1)*split_size]
    
    dfr.save_dict_to_fasta(output_dict, output)
    
    
# =================== main ======================= #
if __name__ == '__main__':
    
    parser = argparse.ArgumentParser(description='identification of fragments')
    parser.add_argument('-i', action='store', dest='fastq_file_path', required=True,
                        help='file to read the sequences')
    parser.add_argument('-f', action='store', dest='fragments_fasta_path', required=True,
                        help='file containing the fragments')
    parser.add_argument('--f2', action='store', dest='other_fragments_fasta_path', required=False,
                        help='optional file containing other fragments', default=None)
    parser.add_argument('-o', action='store', dest='output_path', required=True,
                        help='file to save the result')
    parser.add_argument('-s', action='store', dest='score_threshold', required=False,
                        type=int, default=7, help='minimum alignment score to recognize a fragment')
    
    arg = parser.parse_args()
    

    molecules_analysis(arg.fastq_file_path, arg.fragments_fasta_path, arg.output_path, arg.score_threshold, arg.other_fragments_fasta_path)
    
