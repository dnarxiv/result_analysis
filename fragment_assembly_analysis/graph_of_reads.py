import sys
import graphviz
import string
import random
import matplotlib.pyplot as plt

def read_analysis(analysis_path):
    """
    get data from the analysis file
    """
    input = open(analysis_path, "r")
    lines = input.readlines()
    input.close()
    
    skip_index = lines.index("\n", 4) + 1
    used_lines = lines[skip_index:] #skip the position in molecule data
    
    node_list = ["Start"]
    fragments_name_list = used_lines[0].split(";")[3:-1]
    for fragment_name in fragments_name_list:
        node_list.append(fragment_name)
        
    edge_list = [[]]
    for follow_occurences in used_lines[1:]:
        follower_list = follow_occurences.split(";")
        edge_list[0].append(follower_list[2]) # update follow occurrences for node First
        edge_list.append(follower_list[3:-2])
    
    # add the value for edge First-End
    edge_list[0].append(lines[2].split(";")[1])
    
    return node_list, edge_list


def print_graph(nodes, edges, graph_output_path):

    dot = graphviz.Digraph(comment='')
    
    dot.node("Start", shape="circle")
    dot.node("End", shape="doublecircle")
    
    for i in range(len(nodes)-1):
        for j in range(len(edges[i])):
            weight = int(edges[i][j])
            node_start = nodes[i]
            node_stop = nodes[j+1]
            if weight >= 20:
                edge_name = node_start+node_stop
                if weight >= 1000: label_font_size = "20"
                else: label_font_size = "14"
                
                dot.attr("edge", label=str(weight), fontsize=label_font_size)
                if node_start == "Start" or node_stop == "End":
                    dot.attr("edge", style="dashed")
                else:
                    dot.attr("edge", style="")
                
                if edge_name in string.ascii_uppercase or edge_name == "StartA" or edge_name == nodes[-2]+"End":
                    dot.edge(node_start, node_stop, weight="1", penwidth="3", color="green")
                elif "Adapter" in edge_name:
                    dot.edge(node_start, node_stop, weight="1", penwidth="1", color="red")
                else:
                    dot.edge(node_start, node_stop, weight="1", penwidth="1")

    print(dot.source)
    dot.render(graph_output_path, view=True)

    return



def simu_synthese(n_mol):
    fragment_size = 60
    fail_proba = 0.006
    distribution = {}
    for i in range(n_mol):
        size = 0
        for j in range(fragment_size):
            if random.random() > fail_proba:
                size += 1
            else:
                break
        distribution[size] = distribution.get(size, 0) +1
    
    distribution_list = []
    for i in range(fragment_size):
        distribution_list.append(distribution.get(i, 0))
    plt.plot(range(fragment_size), distribution_list)
    plt.ylim(0, 6500)
    plt.show()
    
# =================== main ======================= #
if __name__ == '__main__':
    
    if len(sys.argv) != 3:
        print("usage : graph_of_reads.py analysis_path graph_output_path")
        sys.exit(1)
    
    analysis_path = sys.argv[1]
    graph_output_path = sys.argv[2]
    simu_synthese(1000000)
    #nodes, edges = read_analysis(analysis_path)
    #print_graph(nodes, edges, graph_output_path)

