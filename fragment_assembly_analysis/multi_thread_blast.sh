#!/bin/bash

# quick script to run multiple parralel blast commands on the dnarxiv machine
# the machine (oboulle@dnarxiv.irisa.fr) has 80 cpu, the script split the input to 8 file and run it on 10 cpu each

# input read file
reads_name=28k_reads.fasta
# database made from the references; /home/dlavenier/ncbi-blast-2.13.0+/bin/makeblastdb -dbtype nucl -in references.fasta -parse_seqids -out blast_references/references_db
database_name=blast_references/references_db

# split the file in 8 parts
file_length=$(wc -l < $reads_name)
# get line number for each part, 2*((x+15)/16) for round up then pair number of line
split_line_nbr=$((2*(($file_length+15)/16)))

split_read_dir=$(echo $reads_name | sed -e 's/.fasta/_split/g')

# create a dir to store the splits
mkdir "$split_read_dir"

# split the reads in 8 parts, store results in the dir
split -l $split_line_nbr -d -a 1 --additional-suffix=.fasta $reads_name "$split_read_dir/split_"

# file to store the global result for the whole read file
result_file=$(echo $reads_name | sed -e 's/.fasta/_blast_result.txt/g')

echo "running blast on 8 files of $split_line_nbr lines"
date # display date

# loop over the 8 split files
for i in $(seq 0 7); do #
    query_name=$split_read_dir/split_$i.fasta
    result_split=$split_read_dir/result_$i.txt
    cpu_number_per_blast_run=10 # 10 cpu has optimal time, increasing or decreasing this number makes blast slower
    # run blastn, get best aligned ref from db for each reads of query, use 10 cpu, use megablast,
    # result in column : /read_id/ref_id/start_in_ref/end_in_ref/start_in_read/end_in_read/
    blast_command='/home/dlavenier/ncbi-blast-2.13.0+/bin/blastn -query $query_name -db $database_name -out $result_split -outfmt "6 qseqid sseqid sstart send qstart qend" -max_hsps 1 -max_target_seqs 1 -task megablast -num_threads $cpu_number_per_blast_run'
    # run the blast command in background and continue the loop immediatly
    eval "$blast_command" &
done

wait # wait for all the blast runs to end

# replace result file from a previous run
cat $split_read_dir/result_*.txt > $result_file

date # display date

echo "Completed !"
exit 0
