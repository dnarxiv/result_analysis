#!/bin/bash

# quick script to rename barcode files, move them out of their dir and merge the files from the same barcode

data_name=data_march_24/fastq_pass


# take all fastq files in dir barcodeX/ and create the concatened file barcodeX.fastq
for d in 0_data/$data_name/* ; do
	if [ -d "$d" ]; then
		gzip -d $d/*.gz # unzip all fastq
	  cat $d/*.fastq > $d.fastq
	fi
done


exit 0

# scp -r oboulle@dnarxiv.irisa.fr:~/Documents/result_analysis/fragment_assembly_analysis/results/*.txt ~/Documents/result_analysis/fragment_assembly_analysis/results/
