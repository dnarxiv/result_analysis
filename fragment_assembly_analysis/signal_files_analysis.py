import sys
import os
import inspect
import argparse
import pod5 # requirement : pip3 install pod5


import fragments_analysis as f_a

currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0, os.path.dirname(parentdir)+"/synthesis_modules")
import dna_file_reader as dfr


def sort_reads_id(input_path, fragments_path):
    """
    extract and sort all reads id sequences for each fragment type
    """

    effective_fragments_list, associated_lines = f_a.get_effective_fragments_list(input_path, fragments_path)

    id_to_block_dict = {} # key=read_id, value = type of associated fragment

    for i, effective_fragments in enumerate(effective_fragments_list):
        if len(effective_fragments) == 1: # only reads with 1 fragment
            for fragment in effective_fragments:
                # extract the id from the read name line
                fragment_id = associated_lines[i][0].split("parent_read_id=")[1].split(" ")[0] # after the parent_read_id and before the next space
                id_to_block_dict[fragment_id] = fragment.name

    return id_to_block_dict


def sort_pod5_file(pod5_file_path, id_to_block_dict, output_dir):
    """
    read and sort all the read signals for each fragment type (fw and rv)
    """
    signal_list_dict = {} # key = fragment type, value = list of pod5 signals

    # get the read signals
    with pod5.Reader(pod5_file_path) as reader:
        for read_record in reader.reads():
            fragment_type = id_to_block_dict.get(str(read_record.read_id), None)
            if fragment_type is not None:
                signal_list_dict[fragment_type] = signal_list_dict.get(fragment_type, [])

                # convert the read_record into a read type
                read = pod5.Read(
                    read_id=read_record.read_id,
                    end_reason=read_record.end_reason,
                    calibration=read_record.calibration,
                    pore=read_record.pore,
                    run_info=read_record.run_info,
                    median_before=read_record.median_before,
                    signal=read_record.signal,
                    read_number=read_record.read_number,
                    start_sample=read_record.start_sample,
                )

                signal_list_dict[fragment_type].append(read)

    # write the signals for each types
    for fragment_type, signal_list in signal_list_dict.items():
        print("writing",len(signal_list),"signals for",fragment_type,"...")
        with pod5.Writer(output_dir+"/"+fragment_type+".pod5") as writer:
            writer.add_reads(signal_list)



# =================== main ======================= #
if __name__ == '__main__':
    
    parser = argparse.ArgumentParser(description='analysis of signal files')
    parser.add_argument('-i', action='store', dest='extraction_input_path', required=True,
                        help='extraction of reads file')
    parser.add_argument('-s', action='store', dest='signals_input_file', required=True,
                        help='pod5 signal file')
    parser.add_argument('-f', action='store', dest='fragments_path', required=True,
                        help='file to read the originals fragments')
    parser.add_argument('-o', action='store', dest='output_dir', required=True,
                        help='dir to save the result pod5 files')


    arg = parser.parse_args()

    id_to_block_dict = sort_reads_id(arg.extraction_input_path, arg.fragments_path)

    sort_pod5_file(arg.signals_input_file, id_to_block_dict, arg.output_dir)


