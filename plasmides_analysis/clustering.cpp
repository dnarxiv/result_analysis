//============================================================================
// Name        : Clustering.cpp
// Author      : Olivier
//============================================================================

//g++ -std=c++11 clustering.cpp -o clustering


#include <map>
#include <algorithm>
#include <vector>
#include "graph.h"
#include <fstream>
#include <set>
#include <chrono>
#include <cmath>
#include <random>

using namespace std;

string reverse_complement(string sequence);
vector<string> read_sequences(string sequences_path, int number_seq);
void clustering(vector<string> fragments_vector, int len_kmer, string output_frag_dir);

int len_kmer = 15; //length of the kmers


int main(int argc,char* argv[]) {
	//args = input.fastq output_frag_dir fragment_size
	if(argc != 4){
		printf("usage : clustering input.fastq output_frag_dir number_seq\n");
		return 1;
	}
	printf("clustering...\n");
	string input_path = argv[1]; // fastq file of the input sequences
	string output_frag_dir = argv[2]; // directory to save the cluster files
	int number_seq = stoi(argv[3]); // number of reads to use
	//int fragment_size = stoi(argv[3]); // original size of the fragments (without spacers)
	vector<string> sequences = read_sequences(input_path, number_seq);

	//printf("Spacer:  %s\n", spacer.c_str());
	//printf("Fragment size:  %d\n", fragment_size);

	//vector<string> fragments_vector = fragments_identification(sequences, spacer, fragment_size);
	//int len_kmer = int(spacer.size()/2); //length of the kmers

	clustering(sequences, len_kmer, output_frag_dir);
	printf("\tcompleted !\n");
	return 0;
}


vector<string> read_sequences(string sequences_path, int number_seq) {
	//read all the sequences from a fastq file and return a shuffled sub list
	vector<string> sequences;
	ifstream file(sequences_path);
	string line;
	int i = 0;
	while (getline(file, line)) { //TODO
		getline(file, line);
		sequences.push_back(line.c_str());
		sequences.push_back(reverse_complement(line.c_str()));
		getline(file, line);
		getline(file, line);
		i++;
	}
	file.close();
	
	if (number_seq = -1) {
		number_seq = sequences.size();
	}
	
	if (number_seq < sequences.size()) {
		// shuffle the reads
		unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
	    std::default_random_engine rng(seed);
		std::shuffle(std::begin(sequences), std::end(sequences), rng);
		
		// select a sub list of a defined number of sequences
		
		auto first = sequences.begin();
	    auto last = sequences.begin() + number_seq;
	    
	    //cout << last << " " << sequences.size() << " seq" << endl;
	 
	    // Copy the element
	    vector<string> sub_sequences(first, last);
		
		return sub_sequences;
	} else {
		return sequences;
	}
}


string reverse_complement(string sequence) {
	//return the reverse complement of a sequence
	reverse(sequence.begin(), sequence.end());
	for (size_t i = 0; i < sequence.length(); ++i) {
		switch (sequence[i]){
	        case 'A':
	        	sequence[i] = 'T';
	            break;
	        case 'C':
	        	sequence[i] = 'G';
	            break;
	        case 'G':
	        	sequence[i] = 'C';
	            break;
	        case 'T':
	        	sequence[i] = 'A';
	            break;
	    }
	}
	return sequence;
}


void clustering(vector<string> fragments_vector, int len_kmer, string output_frag_dir) {

	auto start = chrono::system_clock::now();
	cout << "assign_set_kmer " << fragments_vector.size() << endl;

	map<string, int> list_kmers; // map of pairs kmers;nbr_occurrence in all the fragments
	
	// count the occurrences of each kmer in all fragments
	for(const string& fragment : fragments_vector) {
		int end_range = fragment.size()-len_kmer+1;
		string kmer;
		for(int i=0; i<end_range; i++) {
			kmer = fragment.substr(i, len_kmer);
			list_kmers[kmer]++;
		}
	}
	int min_kmers_threshold = 5; // minimum number of occurrence for a kmer in list_kmers

	// assign a set of kmers for each fragments, filter kmers that apprears less than {min_kmers_threshold} in all fragments
	vector<vector<string>> set_kmers_per_fragment; // list of kmers set, index = fragment index in the list of fragments
	for(const string& fragment : fragments_vector) {
		vector<string> related_kmers; // kmers of this fragment
		int end_range = fragment.size()-len_kmer+1;
		string kmer;
		for(int i=0; i<end_range; i++) {
			kmer = fragment.substr(i, len_kmer);
			if(list_kmers[kmer] >= min_kmers_threshold) {
				related_kmers.push_back(kmer);
			}
		}
		set_kmers_per_fragment.push_back(related_kmers);
	}

	chrono::duration<double> elapsed_seconds = chrono::system_clock::now()-start;
	//cout << "assign kmers: " << elapsed_seconds.count() << "s\n";

	// CONSTRUCT FRAGMENT GRAPH / clustering
	start = chrono::system_clock::now();

	int len_frag = fragments_vector.size();
	cout << "construct_fragment_graph " << set_kmers_per_fragment.size() << endl;
	Graph G(len_frag); // init a graph, node i = fragment of index i
	map<string, vector<int>> kmer_occurence; // map of pairs kmer:(list of index for fragments containing this kmer)

	for(int i=0; i<len_frag; i++) {
		for(const string& kmer : set_kmers_per_fragment[i]) {
			kmer_occurence[kmer].push_back(i);
		}
	}

	// threshold for number of shared kmers between 2 fragments to be linked by an edge
	// a fragment has =~ <fragment_size> overlapping kmers
	float shared_kmers_rate = (1.0/5.0);
	for(int i=0; i<len_frag; i++) {
		
		
		//cout << i << "/" << len_frag << endl;
		vector<int> shared_kmers(len_frag, 0); // vector where shared_kmers[i] = number of shared kmers with fragment i
		for(const string& kmer : set_kmers_per_fragment[i]) {
			for(const int& num_frag : kmer_occurence[kmer]) {
				shared_kmers[num_frag]++;
			}
		}
		int fragment_size = fragments_vector[i].size();
		for(int num_frag=i+1; num_frag<len_frag; num_frag++) {
			int other_fragment_size = fragments_vector[num_frag].size();
			int mean_size = (fragment_size + other_fragment_size) /2; // mean size between the 2 fragments
			//cout << shared_kmers[num_frag] << " " << mean_size*shared_kmers_rate << endl;
			if( abs(other_fragment_size-fragment_size) < 300 && shared_kmers[num_frag] > mean_size*shared_kmers_rate) {
				//an edge is created between 2 fragments if enough kmers are shared
				G.add_edge(i, num_frag);
			}
		}
	}
	
	elapsed_seconds = chrono::system_clock::now()-start;
	//cout << "construct frag graph: " << elapsed_seconds.count() << "s\n";
	start = chrono::system_clock::now();
	
	//PRINT CONNECTED COMPONENTS
	//save the list of clusters fragments
	int k = 0;
	cout << "save clusters" << endl;
    vector<vector<int>> connected_components = G.get_connected_components();
    
    vector<vector<int>> filtered_components = connected_components;//G.filter_low_connected_components(connected_components);
	//cout << "connected components " << connected_components.size() << endl;
    for(const vector<int>& cluster : filtered_components) {
    	
    	if(cluster.size() > 10) {
    		cout << "cluster with " << cluster.size() << endl;
    		ofstream outfile(output_frag_dir+"/f"+to_string(k)+".fastq");
    		for(const int& node : cluster) {
    			outfile << "@s"+to_string(k)+"_"+to_string(node)+"\n"+
    					fragments_vector[node]+"\n+"+"\n+" << endl;
    		}
    		//cout << endl;
    		outfile.close();
    		k++;
    	}
    }
	//cout << k << " clusters" << endl;
    elapsed_seconds = chrono::system_clock::now()-start;
    //cout << "save connected components: " << elapsed_seconds.count() << "s\n";
}





