#!/usr/bin/python3

import os
import sys
import inspect
import argparse
import random
import matplotlib.pyplot as plt
import subprocess
import time

currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)

sys.path.insert(0, os.path.dirname(parentdir)+"/synthesis_modules")
import dna_file_reader as dfr

sys.path.insert(0, os.path.dirname(parentdir)+"/sequencing_modules/")
import reads_consensus_class

sys.path.insert(0, parentdir+"/fragment_assembly_analysis/")
import alignment_global as ag


# dict with plasmide_name, [ start_sequence, expected_length]
filter_plasmide_dict = {"pGEMHE":["GATCTCGAGCTCAAGCTTCG", "GCTGTACAAGTCCGGACTCA", 5200],
                        "pmirGLO":["GATCTACCGGGTAGGGGAGG", "CGTCCGGCGTAGAGGATCGA", 7000],
                        "FLAGHA":["GATCTGCCACCATGGACTAC", "TTCTCTAGGCGCCGGAATTA", 7000],
                        "pcDNA3.1":["GATCTCCCGATCCCCTATGG", "CTGACGTCGACGGATCGGGA", 7000],
                        "HLTV":["AATTCCGCTGAGCAATAACT", "AGGATCCACTGACTATTAAG", 4450],
                        "REP1":["GGTCGATCGACTCTAGAGGA", "GACAAGCTTGCATGCCTGCA", 10000],
                        "pT7TSV5_Bam": ["GATCCTCTAGAGTCGACCTG", "CGAATTCGAGCTCGCCCGGG", 3500],
                        "pT7TSV5_Hin": ["AGCTTGTCTCCCTATAGTGA", "TTCTGCAAAAAGAACAAGCA", 3500]}


def filter_barcodes():
    """
    remove reads with big homopolymeres
    """
    
    for barcode in range(1,8):
        sequences_dict = dfr.read_fastq("data/demultiplexed/barcode0"+str(barcode)+".fastq", score=True)
        filtered_dict = {}
        i, j = 0, 0
        for key, [seq_value, seq_description, seq_score] in sequences_dict.items():
            if not reads_consensus_class.pre_filter(seq_value):
                filtered_dict[key] = [seq_value, seq_description, seq_score]
                i+=1
            j+=1
                
        print(i,j)
        dfr.save_dict_to_fastq(filtered_dict, "data/demultiplexed/barcode0"+str(barcode)+"_filtered.fastq", score=True)



def cluster_reads(reads_path, result_dir):
    """
    puts reads in a family cluster depending on their size and starting sequence
    """
    
    sequences_dict = dfr.read_fastq(reads_path, score=True)

    #interval = 1000 # accept reads of the supposed length +- interval  
    start_seq_size = 12 # subpart of the start sequence to search in reads
    start_seq_interval = 5*start_seq_size # size of the zone to search for the start sequence

    for plasmide_name, [start_sequence, stop_sequence, estimated_length] in filter_plasmide_dict.items():
        filtered_dict = {}
        print("filtering for",plasmide_name,"...")
        kmer_to_search_start = start_sequence[:start_seq_size]
        kmer_to_search_stop = stop_sequence[-start_seq_size:]
        
        for read_name, [sequence, description, score] in sequences_dict.items():
            #filter by length
            #if len(sequence) >= estimated_length-interval and len(sequence) <= estimated_length+interval:
            if kmer_to_search_start in sequence[:start_seq_interval] or kmer_to_search_stop in sequence[-start_seq_interval:]:
                filtered_dict[read_name] = [sequence, description, score]
            elif dfr.reverse_complement(kmer_to_search_start) in sequence[-start_seq_interval:] or dfr.reverse_complement(kmer_to_search_stop) in sequence[:start_seq_interval]:
                filtered_dict[read_name+"_rc"] = [dfr.reverse_complement(sequence), description, score]        

        dfr.save_dict_to_fastq(filtered_dict, result_dir+"/"+plasmide_name+".fastq", score=True)
        print(len(filtered_dict),"reads selected")



def apply_consensus() -> None:


    min_occ = 4
    kmer_size = 35

    result_sequences_dict = {}
    
    #start sequence, stop is a reverse complement
    start_stop_dict = {"pmirGLO": ["GATCTACCGGGTAGGGGAGG", "CGTCCGGCGTAGAGGATCGA"], "pcDNA3.1":["GATCTCCCGATCCCCTATGG","CTGACGTCGACGGATCGGGA"]}

    for plasmide_name, [start_sequence, stop_sequence] in start_stop_dict.items():
        print("consensus for",plasmide_name,"...")
        cluster_file = "results/clusters_bc07/"+plasmide_name+".fastq"
        #cluster_file = "data/demultiplexed/barcode07_filtered.fastq"

        reads_consensus_class.kmer_consensus(cluster_file, "results/consensus_"+plasmide_name+".fasta", start_sequence, dfr.reverse_complement(stop_sequence), kmer_size, min_occ)

    print("done")




if __name__ == "__main__":
    
    # --------- argument part -------------#
    parser = argparse.ArgumentParser(description='apply reads consensus')
    parser.add_argument('-i', action='store', dest='input_path', required=False,
                        help='fastq input file of reads')
    parser.add_argument('-o', action='store', dest='output_dir', required=False,
                        help='directory to save the result fastq files')
    arg = parser.parse_args()
        
    #filter_barcodes()
    cluster_reads(arg.input_path, arg.output_dir)
    
    #apply_consensus()
    
    
    
    

