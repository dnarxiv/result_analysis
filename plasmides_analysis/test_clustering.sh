g++ -std=c++11 clustering.cpp -o clustering
if [ ! $? = 0 ]
then
	echo "compilation error"
	exit 1
fi

barcode="07"
result_dir=results/bc$barcode
rm -rf $result_dir
mkdir $result_dir
./clustering data/demultiplexed/barcode"$barcode".fastq $result_dir -1

#AAAAAAAACCCCCCCC

