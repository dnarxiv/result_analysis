#!/usr/bin/python3

import os
import sys
import argparse
import subprocess


"""
script do download original files from the software heritage API from a csv of rows of files data
"""

def download_files(csv_path: str, output_dir_path: str) -> None:

    with open(csv_path) as input_file:
        line = input_file.readline()
        file_number = 1

        while line != "":
            file_id = line.split(",")[0] # get SH id
            file_name = line.split(",")[2] # get filename

            short_id = file_id.replace("swh:1:cnt:", "") # extract id hash
            numbered_name = "{:02d}".format(file_number) + "_" + file_name # add an ordering number to the name

            wget_cmd = "wget https://archive.softwareheritage.org/api/1/content/sha1_git:"+short_id+"/raw && mv raw "+output_dir_path+"/"+numbered_name
            subprocess.call('/bin/bash -c "$WGET_CMD"', shell=True, env={'WGET_CMD': wget_cmd}) # call the API

            line = input_file.readline()
            file_number += 1



# =================== main =======================#
if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='download files from the software heritage archive')
    parser.add_argument('-i', action='store', dest='input_csv', required=True,
                        help='input csv file of files data')
    parser.add_argument('-o', action='store', dest='output_dir', required=True,
                        help='directory to store the files')


    # ---------- input list ---------------#
    arg = parser.parse_args()

    download_files(arg.input_csv, arg.output_dir)


#wget https://archive.softwareheritage.org/api/1/content/sha1_git:2c1490e287c538529bbb22442ba49503a64b6cd1/raw && mv raw test.png
# get every 200M files
#awk 'NR==1 || NR%200000001==1' all_sorted/all_sorted_00.csv > extracted_sorted_files2.csv
