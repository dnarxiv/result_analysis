
import os
import sys
import inspect

currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0, os.path.dirname(parentdir)+"/synthesis_simulation")
import dna_file_reader as dfr
sys.path.insert(0, parentdir+"/fragment_assembly_analysis")
import alignment_global as align_g


def get_corresponding_couples(original_fragments, consensus_fragments):
    """
    create a list of couples of original/consensus fragments
    gets the corresponding original fragment for each consensus
    """

    # matrix of correspondence scores for each couple of original and consensus fragments
    correspondence_scores = [ [] for k in range(len(consensus_fragments)) ]
    
    for i in range(len(consensus_fragments)):
        for j in range(len(original_fragments)):
            score = get_score(consensus_fragments[i], original_fragments[j])
            correspondence_scores[i].append(score)

    corresponding_couples = []
    
    # select the couple with the highest correspondence score in the rows
    for i in range(len(consensus_fragments)):
        j = correspondence_scores[i].index(max(correspondence_scores[i]))
        if correspondence_scores[i][j] > 20: # filter couples with very bad correspondence
            couple = consensus_fragments[i], original_fragments[j]
            corresponding_couples.append(couple)
    
    return corresponding_couples


def get_score(fragment_A, fragment_B):
    """
    correspondence score: size of the common substring from the start + size from the stop
    perfect match has a score of 2*fragment_size
    1 error has a score of fragment_size-1
    """
    i, j = 0, 0
    while i<len(fragment_A) and i<len(fragment_B) and fragment_B[i] == fragment_A[i]:
        i +=1
    while j<len(fragment_A) and j<len(fragment_B) and fragment_B[::-1][j] == fragment_A[::-1][j]:
        j +=1
    return i+j


def get_errors_mean(corresponding_couples):
    if len(corresponding_couples) == 0: return 0
    
    errors_sum = [0,0,0]
    for consensus_fragment, original_fragment in corresponding_couples:
        alignment_a, alignment_b = align_g.alignment_nw(original_fragment, consensus_fragment)
        i, d, s = align_g.count_errors(alignment_a, alignment_b)
        errors_sum = [errors_sum[0]+i, errors_sum[1]+d, errors_sum[2]+s]
    return [round(k/len(corresponding_couples), 2) for k in errors_sum]
        

def save_results(output_path, nbr_original_fragments, nbr_clusters, nbr_consensus_fragments, nbr_corresponding_couples, errors_mean):

    output = open(output_path, "w")
    output.write("nbr of fragments : "+str(nbr_original_fragments)+"\n")
    output.write("nbr of clusters : "+str(nbr_clusters)+"\n")
    output.write("nbr of consensus : "+ str(nbr_consensus_fragments)+"\n")
    output.write("nbr of corresponding consensus : "+ str(nbr_corresponding_couples)+"\n")
    output.write("average nbr of errors in corresponding consensus : "+ str(errors_mean)+"\n")

    output.close()
    
    
def get_clustering_consensus_data(stored_document_path):
    """
    :return: nbr_fragments, nbr_clusters, nbr_consensus, nbr_corresponding_couples, errors_mean
    """
    original_fragments_path = stored_document_path + "/3_final_fragments.fasta"
    clusters_path = stored_document_path + "/8_clusters"
    consensus_path = stored_document_path + "/9_consensus.fasta"
    
    original_fragments = list(dfr.read_fasta(original_fragments_path).values())
    consensus_fragments = list(dfr.read_fasta(consensus_path).values())
    
    nbr_clusters = len(os.listdir(clusters_path))
    
    corresponding_couples = get_corresponding_couples(original_fragments, consensus_fragments)
    errors_mean = get_errors_mean(corresponding_couples)
    
    return len(original_fragments), nbr_clusters, len(consensus_fragments), len(corresponding_couples), errors_mean
    
    
# =================== main ======================= #
if __name__ == '__main__':
    
    if len(sys.argv) != 5:
        print("usage : clustering_consensus_analysis.py original_fragments_path clusters_path consensus_path output_path")
        sys.exit(1)

    original_fragments_path = sys.argv[1]
    clusters_path = sys.argv[2]
    consensus_path = sys.argv[3]
    output_path = sys.argv[4]
    
    original_fragments = list(dfr.read_fasta(original_fragments_path).values())
    consensus_fragments = list(dfr.read_fasta(consensus_path).values())
    
    nbr_clusters = len(os.listdir(clusters_path))
    
    corresponding_couples = get_corresponding_couples(original_fragments, consensus_fragments)
    errors_mean = get_errors_mean(corresponding_couples)
    
    save_results(output_path, len(original_fragments), nbr_clusters, len(consensus_fragments), len(corresponding_couples), errors_mean)

