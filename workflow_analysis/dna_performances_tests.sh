#!/bin/bash


n_read_list=`seq 10000 1000 10000` #'seq first step last'
container_name=container_i100_f100_V2_cd
document_path=../../workflow_commands/documents_test/img_100.png
workflow_cmd_dir="../../workflow_commands"

(exec $workflow_cmd_dir/dna_workflow.sh -no_read $document_path $container_name) #init a container 

cat $container_name/0/workflow_times.txt >> $container_name/workflow_times_$container_name.txt
rm $container_name/0/workflow_times.txt

for n_read in $n_read_list; do
	echo ______________ n_read : $n_read ______________
	rm -rf $container_name/0/{6*,7*,8*,9*,10*,11*,12*} #remove previous reading files
	(exec $workflow_cmd_dir/dna_read.sh -n_read $n_read $container_name 0 images_results/${n_read}_"$(basename $document_path)")
	python3 save_results.py $container_name/0 $container_name/results_workflow_$container_name.txt
	echo "___" $n_read "reads ___" >> $container_name/workflow_times_$container_name.txt
	cat $container_name/0/workflow_times.txt >> $container_name/workflow_times_$container_name.txt
	rm $container_name/0/workflow_times.txt
done

#-------------- Exit --------------#
echo "___Fin du processus \!___"

exit 0
