import os
import sys
import matplotlib.pyplot as plt


def compute_results(results_path, n_read_max=100000):
    
    if not os.path.isfile(results_path):
        print("error : file not found : "+results_path)
        exit(1)
    
    global source_length_array
    source_length_array = []
    global n_fragments
    global n_read_array
    n_read_array = []
    global n_clusters_array
    n_clusters_array = []
    global n_consensus_array
    n_consensus_array = []
    global n_corresponding_consensus_array
    n_corresponding_consensus_array = []
    global average_errors_array
    average_errors_array = []
    global precision_array
    precision_array = []
    global reading_time_array
    reading_time_array = []
    
    results_file = open(results_path)
    line = results_file.readline()
    while line != "": #read the file
        source_length = results_file.readline().split(" : ")[1].replace("\n","")
        n_fragments = results_file.readline().split(" : ")[1].replace("\n","")  
        n_read = results_file.readline().split(" : ")[1].replace("\n","")
        n_clusters = results_file.readline().split(" : ")[1].replace("\n","")
        n_consensus = results_file.readline().split(" : ")[1].replace("\n","")
        n_corresponding_consensus = results_file.readline().split(" : ")[1].replace("\n","")
        average_errors = results_file.readline().split(" : ")[1].replace("\n","")
        precision = results_file.readline().split(" : ")[1].replace("\n","")
        reading_time = results_file.readline().split(" : ")[1].replace("\n","")
        line = results_file.readline()
        line = results_file.readline()
            
        if int(n_read) > n_read_max: break
        
        if source_length != "None" and n_read != "None":
            source_length_array.append(int(source_length))
            n_read_array.append(int(n_read))
        else:
            continue #skip the line
        
        n_clusters_array.append(int(n_clusters))
        n_consensus_array.append(int(n_consensus))
        n_corresponding_consensus_array.append(int(n_corresponding_consensus))
        #average_errors_array.append(float(average_errors))
        
        if precision != "None":
            precision_array.append(float(precision))
        else: #precision = None -> no result has been found -> 0% precision
            precision_array.append(0)
        
        if reading_time != "None":
            reading_time_array.append(int(reading_time))
        else:
            reading_time_array.append(None)


def plot_precision(color):
    plt.figure(1)
    plt.plot(n_read_array, precision_array, color=color)
    
    plt.xlabel("n_reads")
    plt.ylabel("precision (%)")

    plt.ylim(0,100)


def plot_clustering_consensus(figure_nbr, color):
    plt.figure(4+i)
    #plt.plot(n_read_array, [int(n_fragments) for k in range(len(n_read_array))], color='black')
    plt.axhline(int(n_fragments), color='black', label='fragments')
    
    plt.plot(n_read_array, n_clusters_array, '-', color=color, label='clusters')
    plt.plot(n_read_array, n_consensus_array, '--', color=color, label='consensus')
    plt.plot(n_read_array, n_corresponding_consensus_array, ':', color=color, label='correct_consensus')

    plt.xlabel("n_reads")
    plt.ylabel("number")
    
    plt.legend()
    #plt.ylim(bottom=900)


def plot_average_error_consensus(color):
    plt.figure(2)
    
    plt.plot(n_read_array, average_errors_array, color=color)

    plt.xlabel("n_reads")
    plt.ylabel("average errors consensus")


def plot_reading_time(color): 
    plt.figure(3)
    plt.plot(n_read_array, reading_time_array, color=color)
    
    plt.xlabel("n_reads")
    plt.ylabel("reading_time (s)")

    plt.ylim(bottom=0)
    #plt.ylim(top=1600)


# =================== main ======================= #
if __name__ == '__main__':
    
    if len(sys.argv) < 2:
        print("usage : plot_results.py results_path [results_path_2] [results_path_3] ...")
        sys.exit(1)
    
    colors = ['blue', 'red', 'green', 'yellow']
    
    for i in range(len(sys.argv)-1):
        results_path = sys.argv[i+1]
        compute_results(results_path)
        plot_precision(colors[i])
        plot_clustering_consensus(i, colors[i])

        #plot_average_error_consensus(colors[i])
        plot_reading_time(colors[i])
    


    plt.show()
